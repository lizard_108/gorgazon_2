<?php

    if( ! ( $count = get_field( 'rating_count' ) ) ) {
        $count = '';
    }
    if( ! ( $avg = get_field( 'rating_avg' ) ) ) {
        $avg = '';
    }

    echo get_gg_rating_html( $avg, $count );