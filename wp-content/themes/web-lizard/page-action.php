<?php
/**
 * Template Name: страница "Акция"
 * @package WordPress
 * @subpackage your-clean-template
 */

get_header(); ?>

    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>

            <div class="main-content">

                <div class="actions-page">
                    <?php
                        if ( have_posts() ) : the_post();
                            the_content();
                        else: ?>
                            <p><?php _e('Ничего нет для вывода.'); ?></p>
                        <?php endif;
                    ?>
                </div>

            </div>
        </div>

        <div class="space"></div>
        <?php get_template_part('template-parts/insert1' ); ?>
        <?php get_template_part('template-parts/insert2' ); ?>
        <?php get_template_part('template-parts/insert3' ); ?>
        <?php get_template_part('template-parts/insert4' ); ?>
        <?php get_template_part('template-parts/insert5' ); ?>
    </main>

<?php get_footer(); ?>