<div class="modal-window modal-window--calc js-calc">
	<div class="container">
		<div class="flex">
			<div class="calc--left-side">
				<div class="close-adapt-calc close"></div>
				<div class="h2 no-caps">Калькулятор расчета стоимости газона</div>
				<div class="modal--subtitle">
					Размеры участка
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Площадь участка, м2</span>
						<br>
						<small>не менее 50 кв, менее не работаем</small>
					</div>
					<div class="cell size-66">
						<label class="static-input">
							<input type="text" value="" name="area">
						</label>
						<br>
						<div class="chechbox">
							<input type="checkbox" name="obrez" id="calc-1" value="1">
							<label for="calc-1">
								+5% на обрезки
							</label>
						</div>
					</div>
				</div>
				<div class="modal--subtitle">
					Газон
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Вид газона</span>
					</div>
					<div class="cell size-66 two-radio">
						<div class="radio">
							<input type="radio" name="sort" id="calc-2-1" value="Рулонный">
							<label for="calc-2-1">
								Рулонный
							</label>
						</div>
						<div class="radio">
							<input type="radio" name="sort" id="calc-2-2" value="Посевной">
							<label for="calc-2-2">
								Посевной
							</label>
						</div>
					</div>
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Сорт газона</span>
					</div>
					<div class="cell size-66">
						<select name="sort">
							<option>Выбрать</option>
							<option>Сорт 1</option>
							<option>Сорт 2</option>
							<option>Сорт 4</option>
							<option>Сорт 5</option>
						</select>
					</div>
				</div>
				<!-- <div class="modal--subtitle">
					Услуги
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Укладка газона</span>
					</div>
					<div class="cell size-66">
						<div class="radio">
							<input type="radio" name="ukladka" id="calc-3-1" value="150">
							<label for="calc-3-1">
								Готовое основание
							</label>
						</div>
						<br>
						<div class="radio">
							<input type="radio" name="ukladka" id="calc-3-2" value="300">
							<label for="calc-3-2">
								Рекультивация
							</label>
						</div>
						<br>
						<div class="radio">
							<input type="radio" name="ukladka" id="calc-3-3" value="500">
							<label for="calc-3-3">
								Замена грунта
							</label>
						</div>
					</div>
				</div> -->
				<!-- <div class="group">
					<div class="cell size-33">
						<span>Бесплатный выезд замерщика</span>
					</div>
					<div class="cell size-66 two-radio">
						<div class="radio">
							<input type="radio" name="calc-4" id="calc-4-1">
							<label for="calc-4-1">
								Да
							</label>
						</div>
						<div class="radio">
							<input type="radio" name="calc-4" id="calc-4-2">
							<label for="calc-4-2">
								Нет
							</label>
						</div>
					</div>
				</div>
				<div class="modal--subtitle">
					Доставка
				</div> -->
				<!-- <div class="group">
					<div class="cell size-33">
						<span>Вид доставки</span>
					</div>
					<div class="cell size-66 two-radio">
						<div class="radio">
							<input type="radio" name="dostavka" id="calc-5-1" value="0">
							<label for="calc-5-1">
								Самовывоз
							</label>
						</div>
						<div class="radio">
							<input type="radio" name="dostavka" id="calc-5-2" value="300">
							<label for="calc-5-2">
								Доставка
							</label>
						</div>
					</div>
				</div> -->
				<div class="modal--subtitle">
					Контактная информация
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Телефон*</span>
					</div>
					<div class="cell size-66">
						<label class="static-input">
							<input type="text" value="" placeholder="" class="input--phone">
						</label>
					</div>
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Имя</span>
					</div>
					<div class="cell size-66">
						<label class="static-input">
							<input type="text" value="" placeholder="Введите имя">
						</label>
					</div>					
				</div>
				<div class="space"></div>
				<div class="group">
					<div class="cell size-33">
						<div class="personal-info">
							Нажимая на кнопку оставить комментарий, вы даете согласие на обработку <b>персональных данных</b>
						</div>		
					</div>
					<div class="cell size-66">
						<div class="btn-green">Оставить заявку</div>
					</div>					
				</div>
			</div>
			<div class="calc--right-side">
				<div class="gray-place">
					<div class="calc--close"></div>
					<div class="calc--adapt-close js-close-adapt-calc-results"></div>
					<div class="h3">Результат</div>
					<table class="calc--result-list">
						<tr>
							<td>Площадь участка</td>
							<td id="calc--area">50 м2</td>
						</tr>
						<tr>
							<td>Запас, 5%</td>
							<td id="calc--zapas">-</td>
						</tr>
						<tr>
							<td>Сорт газона</td>
							<td id="calc--sort">Спорт</td>
						</tr>
						<tr>
							<td>Стоимость газона:</td>
							<td id="calc--stoim">450 руб./рулон</td>
						</tr>
						<tr>
							<td>Количество рулонов</td>
							<td id="calc--col-rulons">5 шт</td>
						</tr>
						<!-- <tr>
							<td>Реультивация</td>
							<td id="calc--rueltivation">100 руб</td>
						</tr>
						<tr>
							<td>Укладка</td>
							<td id="calc--ukladka">150 руб</td>
						</tr>
						<tr>
							<td>Накладные расходы</td>
							<td id="calc--naklad">2 250 руб</td>
						</tr>
						<tr>
							<td>Доставка</td>
							<td id="calc--dostavka">-</td>
						</tr> -->
					</table>
				</div>
				<div class="green-place">
					<div class="calc-question js-open-adapt-calc-results"></div>
					<table>
						<tr>
							<td>Итого:</td>
							<td class="final">50 450 руб</td>
						</tr>
					</table>
				</div>
				<div class="green2-place">
					<table>
						<tr>
							<td>Скачать</td>
							<td><a href="">PDF</a></td>
						</tr>
						<tr>
							<td>Отправить результат</td>
							<td><a href="">Mail</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>