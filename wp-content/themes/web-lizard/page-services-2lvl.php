<?php
/**
 * Template Name: "Услуги - 2 уровень"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); ?>

    <main>
        <div class="content-container">
            <?php get_template_part("inc/search") ?>
            <?php get_template_part("inc/breadcrumbs") ?>
            <?php get_template_part('template-parts/services', 'submenu'); ?>

            <div class="main-content">

                <div class="h1"><?php echo get_the_title() ?></div>
                <?php get_template_part('template-parts/page', 'intro') ?>

                <div class="btn-green content-button js-open-callback-form">Оставить заявку</div>
                <div class="space"></div>

                <?php get_template_part('loops/products', '2lvl'); ?>
                <div class="space-40"></div>

                <?php get_template_part('template-parts/insert1'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert2'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert3'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert4'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert5'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert6'); ?>
                <div class="space"></div>

                <?php get_template_part('loops/reviews'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert7'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert8'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/projects', 'slider'); ?>
                <div class="space"></div>

                <?php get_template_part('template-parts/insert10'); ?>
            </div>
        </div>

        <?php get_template_part('template-parts/insert9'); ?>
        <div class="space"></div>

        <?php get_template_part('template-parts/content'); ?>
    </main>

<?php get_footer(); ?>