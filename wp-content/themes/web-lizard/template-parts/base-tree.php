<?php
    // используем wp_list_pages, чтобы показать родительскую и все дочерние страницы (включая все вложенные в них)
    $pages = get_pages();
    if ($pages) {
        $pageids = array();
        foreach ($pages as $page)
            $pageids[]= $page->ID;

        $args=array(
            'title_li' => '',
            'child_of' => 389,
            //'include' =>  $parent . ',' . implode(",", $pageids),
            'sort_column' => 'post_title'
        );
        echo '<ul class="article-list" itemprop="articleBody">';
            wp_list_pages($args);
        echo '</ul>';
    }
