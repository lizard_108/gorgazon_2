<?php

    $tabs_list = [];
    $class_on = '';
    $descr = get_field('service_tab_descr' );

    $class_on_exists = false;

    if( $descr ) {
        $class_on = " class='on'";
        $tabs_list[] = "<li data-open='description'{$class_on}>Описание</li>";
        $class_on_exists = true;
    }

    $prefs = get_field('service_tab_prefs' );
    if( $prefs) {
        $class_on = '';
        if ( ! $class_on_exists ){
            $class_on = " class='on'";
        }

        $tabs_list[] = "<li data-open='preferences'{$class_on}>Преимущества</li>";
        $class_on_exists = true;
    }

    $price = get_field('service_tab_price' );
    if( $price) {
        $class_on ='';
        if ( ! $class_on_exists ){
            $class_on = " class='on'";
        }
        $tabs_list[] = "<li data-open='price'{$class_on}>Стоимость газона</li>";
        $class_on_exists = true;
    }

    $reviews = get_field('service_tab_reviews' );
    if( $reviews) {
        $class_on ='';
        if ( ! $class_on_exists ){
            $class_on = " class='on'";
        }
        $tabs_list[] = "<li data-open='reviews'{$class_on}>Отзывы</li>";
        $class_on_exists = true;
    }

    $ozelen = get_field('service_tab_ozelen' );
    if( $ozelen) {
        $class_on ='';
        if ( ! $class_on_exists ){
            $class_on = " class='on'";
        }
        $tabs_list[] = "<li data-open='Выезд озеленителя'{$class_on}>Выезд озеленителя</li>";
    }

?>

<ul id='servprod-tab-switcher' class="tab-switcher">
    <?php echo implode('', $tabs_list )?>
</ul>


<div class="hidden-sections content-wrap">
    <?php if($descr) : ?>
        <div data-tabname='description' class="section" itemprop="description">
            <?php echo apply_filters( 'the_content', $descr )?>
        </div>
    <?php endif ?>

    <?php if($prefs) : ?>
        <div data-tabname='preferences' class="section">
            <?php echo apply_filters( 'the_content', $prefs )?>
        </div>
    <?php endif ?>

    <?php if($price) : ?>
        <div data-tabname='price' class="section">
            <?php echo apply_filters( 'the_content', $price )?>
        </div>
    <?php endif ?>

    <?php if($reviews) : ?>
        <div data-tabname='reviews' class="section">
            <?php echo apply_filters( 'the_content', $reviews )?>
        </div>
    <?php endif ?>

    <?php if($ozelen) : ?>
        <div data-tabname='ozelenitel' class="section">
            <?php echo apply_filters( 'the_content', $ozelen )?>
        </div>
    <?php endif ?>
</div>