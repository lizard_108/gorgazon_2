<?php
/**
 * Template Name: Общая страница (page.php)
 * @package WordPress
 * @subpackage your-clean-template
 */

get_header(); ?>

    <main>
        <div class="content-container">
            <? include "inc/search.php" ?>
            <? include "inc/breadcrumbs.php" ?>

            <?php get_template_part('template-parts/insert1' ) ?>
            <?php get_template_part('template-parts/insert2' ) ?>

            <div class="main-content content-wrap">

                <div class="h1"><?php echo get_the_title() ?></div>
                <?php get_template_part('template-parts/page', 'intro' ) ?>

                <?php get_template_part('template-parts/content2' ) ?>

            </div>

            <?php get_template_part('template-parts/insert3' ) ?>
            <?php get_template_part('template-parts/insert4' ) ?>

        </div>

        <?php get_template_part('template-parts/insert5' ) ?>
    </main>

<? get_footer();