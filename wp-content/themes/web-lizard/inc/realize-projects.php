<div class="realize-projects">
	<div class="h2 h2-link">Реализованые проекты <a href="">все услуги</a></div>
	<div class="slider--wr">
		<div class="realize-projects--slider">
			<?
			for ($i=0; $i < 6; $i++) { ?>
			<div class="item">
				<div class="image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/project-demo.png" alt="">
				</div>
				<ul class="blog-attributes">
					<li>Укладка</li>
					<li><a href="">85 м<sup>2</sup></a></li>
				</ul>
				<div class="title">Озеленение рулонным газоном крыши БЦ Москва</div>
			</div>
			<?}?>
		</div>
	</div>
</div>
<div class="show-mobile all-list-link"><a href="">Все проекты</a></div>