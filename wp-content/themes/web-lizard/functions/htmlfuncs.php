<?php

function get_gg_extend_menu(){

    $html = "<div class='services-submenu'>
        <div class='services-submenu--title'>Услуги</div>" .

            wp_nav_menu( [
                'menu'  => 'Services',
                'container' => '',
                'menu_class' => 'services-submenu--menu',
                'echo'      => false
            ] ) .

        "</div>";
}

function get_gg_rating_html( $schema = '', $size_class='size-small', $rating='', $count='' ) { // size-middle, size-big

    if ( ! $rating ) {
        $rating = get_field( 'rating_avg' );
    }

    if ( ! $count ) {
        $count = get_field( 'rating_count' );
    }

    $schema_str = "";
    if($schema){
        $schema_str = " itemprop='aggregateRating' itemscope='' itemtype='http://schema.org/AggregateRating'";
    }

    if ( ! ( $rating && $count ) ) {
        $html = "<div class='flex services-product-rating'{$schema_str}>
                <div class='module-stars {$size_class}' data-rating='0' >
                    <div class='module-stars--control'></div>
                </div>
                <div class='services-list-2--reviews'>нет отзывов</div>						
            </div>";

    } else {

        $revtext = get_gg_revtext($count, $schema);

        $html = "<div class='flex services-product-rating'{$schema_str}>
                    <div><strong itemprop='ratingValue'>$rating</strong></div>
                    <div class='module-stars {$size_class}' data-rating='{$rating}' >
                        <div class='module-stars--control'></div>
                    </div>
                    <div class='services-list-2--reviews'>{$revtext}</div>						
                </div>";
    }

    return $html;
}

function get_gg_revtext( $cnt, $schema = '' ) {

    if ( $cnt && count( $cnt ) > 0 ) {
        if ( $schema ) {
            $cnt = "<span itemprop='reviewCount'>{$cnt}</span>";
        }

        $last_dig = (int) substr( $cnt, - 1 );
        if ( $last_dig == 1 ) {
            return "( {$cnt} оценка )";
        } elseif ( $last_dig < 5 && $last_dig > 1 ) {
            return "( {$cnt} оценки )";
        } else {
            return "( {$cnt} оценок )";
        }
    } else {
        return 'нет оценок';
    }
}

function get_gg_price( $id = '' ){

    if(!$id){
        $id = get_the_ID();
    }

    $price_arr = [];

    $priceb = get_field( 'before_price_text', $id );
    if($priceb){
        $price_arr[] = $priceb;
    }
    $price =  get_field( 'price', $id );
    if($price){
        $price_arr[] = $price;
    } else {
        return '';
    }

    $pricea =  get_field( 'after_price_text', $id );
    if($pricea){
        $price_arr[] = $pricea;
    }

    if(!empty($price_arr)){
        return implode( ' ', $price_arr );
    }

    return '';
}

function get_gg_extprice( $id = '' ){

    if(!$id){
        $id = get_the_ID();
    }

    $price_arr = [];

    $priceb = get_field( 'before_extprice_text', $id );
    if($priceb){
        $price_arr[] = $priceb;
    }
    $price =  get_field( 'extprice', $id );
    if($price){
        $price_arr[] = $price;
    } else {
        return '';
    }
    $pricea =  get_field( 'after_extprice_text', $id );
    if($pricea){
        $price_arr[] = $pricea;
    }

    if(!empty($price_arr)){
        return implode( ' ', $price_arr );
    }

    return '';

}


function get_gg_cats_list( $id = '' ) {

    $post_cats = get_the_category( $id );

    $cats_list = '';
    foreach ( $post_cats as $cat ){
        $cats_list .= "<li data-cat='{$cat->slug}'>{$cat->name}</li>";
    }

    return $cats_list;
}