WebP Express 0.17.3. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2020-03-24 13:59:14

*WebP Convert 2.3.0*  ignited.
- PHP version: 7.3.9
- Server software: Apache

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/web-lizard/images/other/gaz-opt.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\other\gaz-opt.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 80
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/web-lizard/images/other/gaz-opt.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\other\gaz-opt.png.webp
- alpha-quality: 80
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: *Exec failed* (return code: 1)
Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version. Result: version: *1.0.3*
Binaries ordered by version number.
- D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "80" -m 6 -low_memory "[doc-root]/wp-content/themes/web-lizard/images/other/gaz-opt.png" -o "[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\other\gaz-opt.png.webp.lossy.webp" 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\other\gaz-opt.png.webp.lossy.webp'
File:      [doc-root]/wp-content/themes/web-lizard/images/other/gaz-opt.png
Dimension: 199 x 215 (with alpha)
Output:    16038 bytes Y-U-V-All-PSNR 41.10 44.66 44.16   41.95 dB
           (3.00 bpp)
block count:  intra4:        164  (90.11%)
              intra16:        18  (9.89%)
              skipped:         9  (4.95%)
bytes used:  header:            245  (1.5%)
             mode-partition:    892  (5.6%)
             transparency:     2551 (58.3 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   10273 |      79 |      58 |       0 |   10410  (64.9%)
 intra16-coeffs:  |     108 |      59 |       0 |       0 |     167  (1.0%)
  chroma coeffs:  |    1668 |      33 |      15 |       0 |    1716  (10.7%)
    macroblocks:  |      93%|       4%|       2%|       2%|     182
      quantizer:  |      14 |      12 |       9 |       8 |
   filter level:  |       4 |       3 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   12049 |     171 |      73 |       0 |   12293  (76.6%)
Lossless-alpha compressed size: 2550 bytes
  * Header size: 95 bytes, image data size: 2455
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   95

Success
Reduction: 81% (went from 84 kb to 16 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: *Exec failed* (return code: 1)
Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version. Result: version: *1.0.3*
Binaries ordered by version number.
- D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
D:\OSPanel\domains\gorgazon-final\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "80" -near_lossless 60 -m 6 -low_memory "[doc-root]/wp-content/themes/web-lizard/images/other/gaz-opt.png" -o "[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\other\gaz-opt.png.webp.lossless.webp" 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\other\gaz-opt.png.webp.lossless.webp'
File:      [doc-root]/wp-content/themes/web-lizard/images/other/gaz-opt.png
Dimension: 199 x 215
Output:    41010 bytes (7.67 bpp)
Lossless-ARGB compressed size: 41010 bytes
  * Header size: 1535 bytes, image data size: 39450
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=3 transform=3 cache=10

Success
Reduction: 53% (went from 84 kb to 40 kb)

Picking lossy
cwebp succeeded :)

Converted image in 592 ms, reducing file size with 81% (went from 84 kb to 16 kb)
