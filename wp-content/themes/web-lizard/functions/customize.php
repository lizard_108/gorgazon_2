<?php

// Добавляем кнопки создания редиректов в админ панель

add_action( 'admin_bar_menu', 'admin_bar_theme_editor_option', 100 );
function admin_bar_theme_editor_option() {
    global $wp_admin_bar;
    if ( !is_super_admin() || !is_admin_bar_showing() )
        return;

    if( current_user_can('manage_options') && is_admin() ){
        $wp_admin_bar->add_menu(
            array( 'id' => 'page-redirects',
                'title' => __('Сформировать редиректы'),

            )
        );
    }
}

// выводим в доп поле список существующих меню
add_filter('acf/load_field/name=extend_menu', 'my_acf_load_field_sidebar_menu');
function my_acf_load_field_sidebar_menu( $field ) {

    $terms = get_terms( 'nav_menu' , 'hide_empty=0');

    if( $terms && ! is_wp_error($terms) ){

        foreach( $terms as $term ){
            $field['choices'][$term->term_id] = $term->name;
        }
    }

    return $field;
}

add_filter( 'intermediate_image_sizes', 'delete_intermediate_image_sizes' );
function delete_intermediate_image_sizes( $sizes ){
    // размеры которые нужно удалить
    return array_diff( $sizes, [
        'medium_large',
        'large',
        '1536x1536',
        '2048x2048',
    ] );
}

/*
// вывод меток для страниц
add_action('admin_init','true_apply_tags_for_pages');
function true_apply_tags_for_pages(){
    add_meta_box( 'tagsdiv-post_tag', 'Теги', 'post_tags_meta_box', 'page', 'side', 'normal' ); // сначала добавляем метабокс меток
    register_taxonomy_for_object_type('post_tag', 'page'); // затем включаем их поддержку страницами wp
}

add_filter('request', 'true_expanded_request_post_tags');
function true_expanded_request_post_tags($q) {
    if (isset($q['tag'])) // если в запросе присутствует параметр метки
        $q['post_type'] = array('post', 'page');
    return $q;
}*/

//убираем теги p
//remove_filter( 'the_content', 'wpautop' );
