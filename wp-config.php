<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', "gorgazon" );


/** Имя пользователя MySQL */
define( 'DB_USER', "root" );


/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', "" );


/** Имя сервера MySQL */
define( 'DB_HOST', "localhost" );


/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );


/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~Faq~0IYHjJx9p{(+{-dc(Wmks?7A|/M%}.Wx CLnc~*Y1n?K%t}N{H%;?tv0VM.' );

define( 'SECURE_AUTH_KEY',  '*yt)cG7Z9Qq`Oj1Knt8J$#znEV8GA(/T,esNT#zA>n`k;5q7=rr@uDi3!4U9;x=8' );

define( 'LOGGED_IN_KEY',    '|SR=LL#o!ETHMsHMNP?[$+juDv~jE;-eYLR~Iz.2QI|F~ V6 BSK^dn #,Y2}hj%' );

define( 'NONCE_KEY',        'bWYV&OCN)8(6n,6;6<gnw9echW$W;*)1<x[}_m8*hX]O%*sFg>(}:gSc0k7~&@W{' );

define( 'AUTH_SALT',        ']Z9-6Ku}Wn/Fh,P=+Y;UG~:d yH)2,,#sPj]!prh1sg-(M_MOc6{(i6az9?4X=`K' );

define( 'SECURE_AUTH_SALT', '/-xep03ht8xNz%ot%&u]9AOXLG@=2yp`~*w)x5J~X{}$R$vf<L7+oSTdLJ{M@u`%' );

define( 'LOGGED_IN_SALT',   '|x=?br1S*9,IY)Bfr1(]l_6bn~`a~[>|^F&=T}O3Q1+IkjfnhO5q{jGj|fXFOlHV' );

define( 'NONCE_SALT',       'n.->EI|cF-^R;@ygV1a&B)^m`xq&J^ ?U>KT&MR=]uB-je.[j*{7:MlQpU,H{(5L' );


/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';


/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
