<div class="modal-window modal-window--order-plitka add-comment--fields-place">
	<div class="close"></div>
	<form id="callback-order-plitka-form">
		<div class="callback--subtitle">Заказать плитку</div>
		<div class="callback--sub-subtitle">Для отправки заказа, пожалуйста, заполните необходимые поля:</div>
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div  class="label">Ваш телефон*</div>
                    <input id='plitka-order-phone' type="text" placeholder="+7 (___) ___-__-__" class="input--phone">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div class="label">Площадь участка (в кв.м.)*</div>
					<input id='plitka-order-square' type="text" placeholder="">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div class="label">Ваше имя*</div>
					<input id='plitka-order-name' type="text" placeholder="">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<div class="textarea-input">
					<textarea id='plitka-order-comment' name="" placeholder="Комментарий"></textarea>
				</div>
			</div>
		</div>
		<div class="group callback--submit-place">
			<div class="cell size-100">
				<div class="btn-green">Оформить заказ</div>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<small>Нажимая на кнопку отправить, вы даете согласие на обработку <a href="">персональных данных</a></small>
			</div>
		</div>
	</form>
	<div class="modal-window--message-after-send">
		<p>Спасибо!</p>
		<p>Наши менеджеры свяжутся с Вами в ближайшее время</p>
		<div class="btn-green js-close-modal-windows">Ок</div>
	</div>
</div>