<?php
/**
 * Template Name: Доставка - 1 уровень
 * @package WordPress
 * @subpackage your-clean-template
 */

get_header(); ?>
    <script src="<?=get_stylesheet_directory_uri()?>/js/base-search.js"></script>
    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>
            <div class="main-content">
                <div class="h1 no-caps"><?php the_title() ?></div>
                <div class="maintext">
                    <?php get_template_part('template-parts/page', 'intro') ?>
                </div>

                <?php get_template_part('template-parts/insert1' ) ?>

                <div class="space"></div>

                <div class="h2 no-caps">Ориентировочная стоимость доставки рулонного газона</div>

                <div class="page-search">
                    <input type="text" placeholder="Поиск" id="spterm" name="spterm">
                    <div class="submit"></div>
                </div>

                <div class="search-reset"><a href="" class="js-search-reset">Сбросить поиск</a></div>
                <br>
                <br>

                <div id="content">
                    <?php get_template_part('loops/delivery-1lvl' );?>
                    <div class="space"></div>
                    <?php get_template_part('template-parts/insert2' );?>
                    <div class="space"></div>
                    <?php get_template_part('template-parts/insert3' );?>
                </div>
            </div>
        </div>

        <?php get_template_part('template-parts/insert4' );?>
        <div class="space"></div>
        <?php get_template_part('template-parts/content' );?>
    </main>

<? get_footer();