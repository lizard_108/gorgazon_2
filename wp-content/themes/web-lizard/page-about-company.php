<?php
/**
 * Template Name: страница "О компании"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>
        <?php get_template_part( 'template-parts/insert1' )?>

		<div class="main-content">
			<div class="h1">О компании Горгазон</div>
            <?php get_template_part( 'template-parts/page', 'intro' )?>
			<p>У нас Вы можете купить <b>рулонный газон</b>, а также заказать устройство <b>посевного газона</b>. А еще мы предлагаем недорогую и оперативную <b>доставку газона</b> по европейской части России. Мы принципиально не зарабатываем на доставке нашего газона.</p>
		</div>
        <?php get_template_part( 'template-parts/insert2' )?>
    </div>

	<div class="about-company-prevelegios">
		<div class="h2 no-caps">Бесспорными преимуществами газонов Горгазон являются:</div>
		<div class="group">
			<div class="cell size-50">
				<img src="/wp-content/themes/web-lizard/images/svg/about-1.svg">
                <div class='h3'>Контроль качества</div>
				<p>Продажа газонов осуществляется только после контроля качества, мы не можем себе позволить опорочить свою репутацию! Если вы купили наши газоны, то можете смело рассчитывать на превосходный результат использования такого покрытия.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-2.svg">
                <div class='h3'>Собственные производственные мощности</div>
				<p>Это материально – техническая база предприятия: специально обрабатываемые поля, на которых выращивается готовый газон только высшего качества; спецтехника по посеву, уходу и срезу готового материала; продуманная система орошения.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-3.svg">
                <div class='h3'>Надежные поставщики</div>
				<p>Наши поставщики, в свою очередь, это отлично зарекомендовавшиеся на мировом рынке производители семян Jacklin Seeds, Seed Research, известные не только в США, но и за ее пределами.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-4.svg">
                <div class='h3'>Международные стандарты</div>
				<p>Процесс производства газонов сертифицирован по международным стандартам ISO 9000 и ISO 14000, что немаловажно для соответствия запросам самых требовательных клиентов России, стремящихся к поистине мировым нормативам.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-5.svg">
                <div class='h3'>Обработка заказов</div>
				<p>Оперативная обработка заказов не допустит проволочек и задержек — мы очень ценим ваше время, поэтому готовы работать на протяжении немыслимо увеличенного рабочего дня: с 9 до 23 часов!</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-6.svg">
                <div class='h3'>Передовые технологии производства</div>
				<p>Делая упор на специализированном современном оборудовании, наша компания беспроигрышно опережает конкурентов.</p>
			</div>
		</div>
	</div>

	<div class="content-container">
        <div class="space"></div>
        <?php get_template_part( 'template-parts/insert3' )?>
        <div class="space"></div>
        <?php get_template_part( 'template-parts/reviews' )?>
        <?php get_template_part( 'template-parts/insert4' )?>
	</div>

    <?php get_template_part( 'template-parts/insert5' )?>
	<?php get_template_part('template-parts/content' );?>
</main>

<? get_footer(); // подключаем footer.php ?>