<?php
/**
 * Template Name: страница "Продукция - 2 уровень"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>
        <?php get_template_part('template-parts/products', 'submenu');?>

		
		<div class="main-content">
			<div class="h1"><?php echo get_the_title() ?></div>
            <?php get_template_part('template-parts/page', 'intro') ?>

            <?php get_template_part('template-parts/download-price', 'block');?>

            <?php get_template_part('loops/products', '2lvl');?>

            <?php get_template_part('template-parts/insert1');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert2');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert3');?>
			<div class="space"></div>

            <?php get_template_part('template-parts/insert4');?>
			<div class="space"></div>

            <?php get_template_part('template-parts/insert5');?>
            <div class="space"></div>

            <?php get_template_part('loops/reviews'); ?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert6');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert7');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert8');?>

		</div>
	</div>
    <?php get_template_part('template-parts/insert9');?>
    <div class="space"></div>

	<?php get_template_part('template-parts/content' );?>
</main>

<? get_footer(); // подключаем footer.php ?>