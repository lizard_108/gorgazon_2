WebP Express 0.17.3. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2020-03-19 19:26:02

*WebP Convert 2.3.0*  ignited.
- PHP version: 7.1.32
- Server software: Apache

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/web-lizard/images/icons-s54aedfc28b.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\icons-s54aedfc28b.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 80
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/web-lizard/images/icons-s54aedfc28b.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\icons-s54aedfc28b.png.webp
- alpha-quality: 80
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: *Exec failed* (return code: 1)
Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version. Result: version: *1.0.3*
Binaries ordered by version number.
- D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "80" -m 6 -low_memory "[doc-root]/wp-content/themes/web-lizard/images/icons-s54aedfc28b.png" -o "[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\icons-s54aedfc28b.png.webp.lossy.webp" 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\icons-s54aedfc28b.png.webp.lossy.webp'
File:      [doc-root]/wp-content/themes/web-lizard/images/icons-s54aedfc28b.png
Dimension: 170 x 476 (with alpha)
Output:    23920 bytes Y-U-V-All-PSNR 44.60 40.97 40.95   43.02 dB
           (2.36 bpp)
block count:  intra4:        209  (63.33%)
              intra16:       121  (36.67%)
              skipped:        49  (14.85%)
bytes used:  header:            456  (1.9%)
             mode-partition:   1097  (4.6%)
             transparency:     7473 (65.5 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    8391 |      11 |       3 |       0 |    8405  (35.1%)
 intra16-coeffs:  |     174 |       1 |       3 |       1 |     179  (0.7%)
  chroma coeffs:  |    6036 |     100 |     108 |      12 |    6256  (26.2%)
    macroblocks:  |      86%|       1%|       1%|      12%|     330
      quantizer:  |      15 |      12 |       9 |       8 |
   filter level:  |      63 |       3 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   14601 |     112 |     114 |      13 |   14840  (62.0%)
Lossless-alpha compressed size: 7472 bytes
  * Header size: 245 bytes, image data size: 7227
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   96

Success
Reduction: 62% (went from 62 kb to 23 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: *Exec failed* (return code: 1)
Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version. Result: version: *1.0.3*
Binaries ordered by version number.
- D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
D:\OSPanel\domains\gorgazon\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "80" -near_lossless 60 -m 6 -low_memory "[doc-root]/wp-content/themes/web-lizard/images/icons-s54aedfc28b.png" -o "[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\icons-s54aedfc28b.png.webp.lossless.webp" 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content\themes\web-lizard\images\icons-s54aedfc28b.png.webp.lossless.webp'
File:      [doc-root]/wp-content/themes/web-lizard/images/icons-s54aedfc28b.png
Dimension: 170 x 476
Output:    37214 bytes (3.68 bpp)
Lossless-ARGB compressed size: 37214 bytes
  * Header size: 1932 bytes, image data size: 35257
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM
  * Precision Bits: histogram=3 transform=3 cache=10

Success
Reduction: 41% (went from 62 kb to 36 kb)

Picking lossy
cwebp succeeded :)

Converted image in 477 ms, reducing file size with 62% (went from 62 kb to 23 kb)
