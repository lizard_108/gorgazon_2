<?  // faq loop

    $args = array(
        'post_type' => 'faq',
        'posts_per_page' => '-1',
        'post_status' => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
    );

    $posts = new WP_Query($args);

    if ($posts->have_posts()) : while ($posts->have_posts()) : $posts->the_post();

        get_template_part('loop-parts/faqlist', 'item');

    endwhile;
        wp_reset_postdata();
    else: ?>

        <p><?php _e('Нет записей'); ?></p>

    <?php endif;