<?php
/**
 * Template Name: страница "Продукция - 1 уровень"
 * @package WordPress
 * @subpackage your-clean-template
 */

get_header(); ?>

    <main>
        <div class="content-container">
            <? include "inc/search.php" ?>
            <? include "inc/breadcrumbs.php" ?>
            <div class="main-content">
                <div class="h1"><?php the_title()?></div>
                <?php get_template_part('template-parts/page', 'intro') ?>

                <div class="services-list">
                    <?php get_template_part('loops/products', '1lvl'); ?>
                </div>
                <div class="space"></div>
                <?php get_template_part('template-parts/insert1');?>
                <div class="space"></div>
                <?php get_template_part('template-parts/insert2');?>
            </div>
        </div>

        <?php get_template_part('template-parts/insert3');?>
        <?php get_template_part('template-parts/content');?>
    </main>

<? get_footer();