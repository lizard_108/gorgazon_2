<?php
/**
 * Template Name: страница "База знаний - единичая страница "
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); ?>

    <main>


        <div class="content-container">

            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>

            <?php get_template_part("template-parts/insert1" );?>

            <?php get_template_part("template-parts/insert2" );?>


            <div class="main-content content-wrap" itemscope itemtype="http://schema.org/Article">

                <div class="h1 no-caps" itemprop="headline"><?php the_title()?></div>

                <?php get_template_part('template-parts/page', 'intro') ?>

                <?php get_template_part("template-parts/rating" );?>

                <?php get_template_part("template-parts/content-bz" );?>

            </div>

            <?php get_template_part("template-parts/insert3" );?>

            <?php get_template_part("template-parts/insert4" );?>

        </div>

        <?php get_template_part("template-parts/insert5" );?>
    </main>

<? get_footer(); // подключаем footer.php ?>