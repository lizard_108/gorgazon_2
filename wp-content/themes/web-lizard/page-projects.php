<?php
/**
 * Template Name: Проекты - страница списка
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>
        <?php get_template_part('template-parts/insert1' );?>
		<div class="main-content">
			<div class="h1 no-caps">Реализованые проекты</div>
            <?php get_template_part('template-parts/page', 'intro' ) ?>

			<div class="group realize-projects-list">
				<?
				for ($i=0; $i < 12; $i++) { ?>
				<div class="cell size-50">
					<div class="image">
						<img src="<?=get_stylesheet_directory_uri()?>/images/other/project-demo.png" alt="">
					</div>
					<ul class="blog-attributes">
						<li>Укладка</li>
						<li><a href="">85 м<sup>2</sup></a></li>
					</ul>
					<div class="title">Озеленение рулонным газоном крыши БЦ Москва</div>
				</div>
				<?}?>
			</div>
			<div class="blog-controls">
				<div class="blog-controls--show-more">Загрузить еще</div>
				<ul class="blog-controls--pagination">
					<li>1</li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
				</ul>
			</div>
		</div>
        <?php get_template_part('template-parts/insert2' );?>
	</div>

    <?php get_template_part('template-parts/insert3' );?>
</main>

<? get_footer(); // подключаем footer.php ?>