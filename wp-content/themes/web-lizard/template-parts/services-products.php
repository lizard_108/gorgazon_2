<?php
// services and products loops

$args = array(
    'post_type'         => 'page',
    'posts_per_page'    => '-1',
    'post_status'       => 'publish',
    'order'             => 'ASC',
    'post_parent'       => get_the_ID(),
    'fields'            => 'ids'
);

$services = new WP_Query($args);

$html = '';

foreach ( $services->posts as $serv ) {

    $title = get_the_title( $serv );
    $img_url = get_the_post_thumbnail_url( $serv );
    $plink = get_permalink( $serv );
    $descr = get_post_meta( $serv, 'service_short_descr', true );
    $price = get_post_meta( $serv, 'service_price', true );
    if($price){
        $price .= '<br><small>Цены указаны с НДС 20%</small>';
    }

    $html .= "<div class='services-list--item'>
					<div class='services-list--img' style='background-image: url({$img_url})'><a href=''></a></div>
					<div class='service-list--text'>
						<div class='h3'>{$title} </div>
						<div class='service-list--description'>{$descr}</div>
						<div class='show-more-link'><a href='{$plink}'>Подробнее</a></div>
					</div>
					<div class='service-list--price'>
                        {$price}
					</div>
				</div>";
}
echo $html;

