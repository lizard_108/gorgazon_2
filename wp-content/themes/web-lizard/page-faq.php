<?php
/**
 * Template Name: Вопрос-ответ
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		<div class="h1 no-caps">Вопрос ответ</div>

		<ul class="tab-switcher">
			<li data-open="1" class="on">Все</li>
			<li data-open="2">Товары</li>
			<li data-open="3">Услуги</li>
		</ul>

		<div class="hidden-sections">
			<div class="section">
				<div class="faq--list">
                    <?php get_template_part( 'loops/faq' ) ?>
				</div>
			</div>
		</div>
		<div class="space"></div>
		<div class="space"></div>
	</div>

	
	<?include "inc/question-form.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>