<?php
/**
 * Template Name: Доставка - 2 уровень
 * @package WordPress
 * @subpackage your-clean-template
 */

get_header(); ?>

    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>
            <div class="main-content">
                <div class="h1 no-caps">Московская область</div>
                <div class="maintext">

                </div>
                <br>

                <div class="page-search">
                    <input type="text" placeholder="Введите название округа">
                    <div class="submit"></div>
                </div>

                <div class="h2 no-caps">Районы Московской области</div>
                <div class="delivery-list">
                    <div class="group">
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">Волоколамский район</a></li>
                                <li><a href="">Дмитровский район</a></li>
                                <li><a href="">Зарайский район</a></li>
                                <li><a href="">Каширский район</a></li>
                                <li><a href="">Коломенский район</a></li>
                                <li><a href="">Ленинский район</a></li>
                                <li><a href="">Луховицкий район</a></li>
                                <li><a href="">Можайский район</a></li>
                                <li><a href="">Наро-Фоминский район</a></li>
                                <li><a href="">Одинцовский район</a></li>
                                <li><a href="">Орехово-Зуевский район</a></li>
                                <li><a href="">Подольский район</a></li>
                                <li><a href="">Раменский район</a></li>
                                <li><a href="">Сергиево-Посадский район</a></li>
                                <li><a href="">Серпуховский район</a></li>
                                <li><a href="">Ступинский район</a></li>
                                <li><a href="">Чеховский район</a></li>
                                <li><a href="">Шаховской район</a></li>
                            </ul>
                        </div>
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">Воскресенский район</a></li>
                                <li><a href="">Егорьевский район</a></li>
                                <li><a href="">Истринский район</a></li>
                                <li><a href="">Клинский район</a></li>
                                <li><a href="">Красногорский район</a></li>
                                <li><a href="">Лотошинский район</a></li>
                                <li><a href="">Люберецкий район</a></li>
                                <li><a href="">Мытищинский район</a></li>
                                <li><a href="">Ногинский район</a></li>
                                <li><a href="">Озёрский район</a></li>
                                <li><a href="">Павлово-Посадский район</a></li>
                                <li><a href="">Пушкинский район</a></li>
                                <li><a href="">Рузский район</a></li>
                                <li><a href="">Серебряно-Прудский район</a></li>
                                <li><a href="">Солнечногорский район</a></li>
                                <li><a href="">Талдомский район</a></li>
                                <li><a href="">Шатурский район</a></li>
                                <li><a href="">Щёлковский район</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="space"></div>
                <div class="h2 no-caps">Приволжский федеральный округ</div>
                <div class="delivery-list">
                    <div class="group">
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">город Москва</a></li>
                                <li><a href="">Московская область</a></li>
                                <li><a href="">Белгородская область</a></li>
                                <li><a href="">Брянская область</a></li>
                                <li><a href="">Владимирская область</a></li>
                                <li><a href="">Воронежская область</a></li>
                                <li><a href="">Ивановская область</a></li>
                                <li><a href="">Калужская область</a></li>
                                <li><a href="">Костромская область</a></li>
                            </ul>
                        </div>
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">Курская область</a></li>
                                <li><a href="">Липецкая область</a></li>
                                <li><a href="">Орловская область</a></li>
                                <li><a href="">Рязанская область</a></li>
                                <li><a href="">Смоленская область</a></li>
                                <li><a href="">Тамбовская область</a></li>
                                <li><a href="">Тверская область</a></li>
                                <li><a href="">Тульская область</a></li>
                                <li><a href="">Ярославская область</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="space"></div>
                <div class="h2 no-caps">Южный федеральный округ</div>
                <div class="delivery-list">
                    <div class="group">
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">город Москва</a></li>
                                <li><a href="">Московская область</a></li>
                                <li><a href="">Белгородская область</a></li>
                                <li><a href="">Брянская область</a></li>
                                <li><a href="">Владимирская область</a></li>
                                <li><a href="">Воронежская область</a></li>
                                <li><a href="">Ивановская область</a></li>
                                <li><a href="">Калужская область</a></li>
                                <li><a href="">Костромская область</a></li>
                            </ul>
                        </div>
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">Курская область</a></li>
                                <li><a href="">Липецкая область</a></li>
                                <li><a href="">Орловская область</a></li>
                                <li><a href="">Рязанская область</a></li>
                                <li><a href="">Смоленская область</a></li>
                                <li><a href="">Тамбовская область</a></li>
                                <li><a href="">Тверская область</a></li>
                                <li><a href="">Тульская область</a></li>
                                <li><a href="">Ярославская область</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="space"></div>
                <div class="h2 no-caps">Северо-Западный федеральный округ</div>
                <div class="delivery-list">
                    <div class="group">
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">город Москва</a></li>
                                <li><a href="">Московская область</a></li>
                                <li><a href="">Белгородская область</a></li>
                                <li><a href="">Брянская область</a></li>
                                <li><a href="">Владимирская область</a></li>
                                <li><a href="">Воронежская область</a></li>
                                <li><a href="">Ивановская область</a></li>
                                <li><a href="">Калужская область</a></li>
                                <li><a href="">Костромская область</a></li>
                            </ul>
                        </div>
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">Курская область</a></li>
                                <li><a href="">Липецкая область</a></li>
                                <li><a href="">Орловская область</a></li>
                                <li><a href="">Рязанская область</a></li>
                                <li><a href="">Смоленская область</a></li>
                                <li><a href="">Тамбовская область</a></li>
                                <li><a href="">Тверская область</a></li>
                                <li><a href="">Тульская область</a></li>
                                <li><a href="">Ярославская область</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="space"></div>
                <div class="h2 no-caps">Северо-Кавказский федеральный округ</div>
                <div class="delivery-list">
                    <div class="group">
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">город Москва</a></li>
                                <li><a href="">Московская область</a></li>
                                <li><a href="">Белгородская область</a></li>
                                <li><a href="">Брянская область</a></li>
                                <li><a href="">Владимирская область</a></li>
                                <li><a href="">Воронежская область</a></li>
                                <li><a href="">Ивановская область</a></li>
                                <li><a href="">Калужская область</a></li>
                                <li><a href="">Костромская область</a></li>
                            </ul>
                        </div>
                        <div class="cell size-50">
                            <ul class="article-list">
                                <li><a href="">Курская область</a></li>
                                <li><a href="">Липецкая область</a></li>
                                <li><a href="">Орловская область</a></li>
                                <li><a href="">Рязанская область</a></li>
                                <li><a href="">Смоленская область</a></li>
                                <li><a href="">Тамбовская область</a></li>
                                <li><a href="">Тверская область</a></li>
                                <li><a href="">Тульская область</a></li>
                                <li><a href="">Ярославская область</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <?php get_template_part('template-parts/insert2' );?>

                <div class="space"></div>


                <?php get_template_part('template-parts/insert3' );?>
            </div>
        </div>

        <?include "inc/question-form.php"?>
        <?php get_template_part('template-parts/content' );?>
    </main>

<? get_footer();