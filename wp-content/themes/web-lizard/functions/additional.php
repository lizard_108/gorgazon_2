<?php

$default_args = [
    'shopname' => 'Горгазон',
    'company' => 'ООО Агора',
    'categories' => [], // [ id => parent_id ]
    'products' => [], // [ id, name, url, price_sale, price, cat_id, images[], descr ]
];

function generateYML( $args ){

    $out = '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n";
    $out .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . "\r\n";
    $out .= '<shop>' . "\r\n";

// Короткое название магазина, должно содержать не более 20 символов.
    $out .= "<name>{$args['shopname']}</name>" . "\r\n";

// Полное наименование компании, владеющей магазином.
    $out .= "<company>ООО «Агора»</company>" . "\r\n";

// URL главной страницы магазина.
    $out .= '<url>' . home_url() . '</url>' . "\r\n";

// Список курсов валют магазина.
    $out .= '<currencies>' . "\r\n";
    $out .= '<currency id="RUR" rate="1"/>' . "\r\n";
    $out .= '</currencies>' . "\r\n";

// Список категорий магазина:
// id     - ID категории.
// parent - ID родительской категории.
// name   - Название категории.

    $out .= '<categories>' . "\r\n";
    foreach ($args['categories'] as $key => $cat) {

        $id_str = ' id="' . $key . '"';
        if( $cat ) {
            $parent_str = ' parentId="' . $cat . '"';
        } else {
            $parent_str = '';
        }

        $out .= "<category{$id_str}{$parent_str}>" . get_the_title($key) . "</category>\r\n";

    }

    $out .= '</categories>' . "\r\n";

// Вывод товаров:
    //$prod = [ id, name, url, price_sale, price, cat_id, images[], descr ]
    $out .= '<offers>' . "\r\n";
    foreach ($args['products'] as $prod) {

        if ( empty($prod['price_sale']) && empty($prod['price'] ) ) {
            continue;
        }

        $out .= '<offer id="' . $prod['id'] . '">' . "\r\n";

        $out .= '<url>' . $prod['url'] . '</url>' . "\r\n";
        if (!empty($prod['price_sale'])) {
            $out .= '<price>' . $prod['price_sale'] . '</price>' . "\r\n";
            $out .= '<oldprice>' . $prod['price'] . '</oldprice>' . "\r\n";
        } else {
            $out .= '<price>' . $prod['price'] . '</price>' . "\r\n";
        }

        // Валюта товара.
        $out .= '<currencyId>RUR</currencyId>' . "\r\n";

        // ID категории.
        $out .= "<categoryId>{$prod['cat_id']}</categoryId>\r\n";

        // Изображения товара, до 10 ссылок.
        $i = 0;
        foreach ($prod['images'] as $img) {
            if ($i++ > 9) {
                break;
            }
            $out .= "<picture>{$img}</picture>\r\n";
        }

        // Название товара.
        $out .= "<name>{$prod['name']}</name>\r\n";

        // Описание товара, максимум 3000 символов.
        if (!empty($prod['descr'])) {
            $out .= '<description>' . stripslashes($prod['descr']) . '</description>' . "\r\n";
        }

        $out .= '</offer>' . "\r\n";
    }

    $out .= '</offers>' . "\r\n";
    $out .= '</shop>' . "\r\n";
    $out .= '</yml_catalog>' . "\r\n";

    $filename = get_stylesheet_directory() . '/data/outputs/gg_products.yml';
    file_put_contents( $filename,  $out );

}



/**
 * Получает информацию обо всех зарегистрированных размерах картинок.
 *
 * @global $_wp_additional_image_sizes
 * @uses   get_intermediate_image_sizes()
 *
 * @param  boolean [$unset_disabled = true] Удалить из списка размеры с 0 высотой и шириной?
 * @return array Данные всех размеров.
 */
function get_image_sizes( $unset_disabled = true ) {
    $wais = & $GLOBALS['_wp_additional_image_sizes'];

    $sizes = array();

    foreach ( get_intermediate_image_sizes() as $_size ) {
        if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
            $sizes[ $_size ] = array(
                'width'  => get_option( "{$_size}_size_w" ),
                'height' => get_option( "{$_size}_size_h" ),
                'crop'   => (bool) get_option( "{$_size}_crop" ),
            );
        }
        elseif ( isset( $wais[$_size] ) ) {
            $sizes[ $_size ] = array(
                'width'  => $wais[ $_size ]['width'],
                'height' => $wais[ $_size ]['height'],
                'crop'   => $wais[ $_size ]['crop'],
            );
        }

        // size registered, but has 0 width and height
        if( $unset_disabled && ($sizes[ $_size ]['width'] == 0) && ($sizes[ $_size ]['height'] == 0) )
            unset( $sizes[ $_size ] );
    }

    return $sizes;
}