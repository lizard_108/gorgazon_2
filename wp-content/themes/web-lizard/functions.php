<?php

require_once 'functions/customize.php';
require_once 'functions/redirects.php';
require_once 'functions/htmlfuncs.php';
require_once 'functions/ajaxfuncs.php';
require_once 'functions/additional.php';


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/css/theme.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
//    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }


}

add_action( 'admin_enqueue_scripts', 'theme_adm_enqueue_styles' );
function theme_adm_enqueue_styles() {
    if( current_user_can('manage_options') ){
        wp_enqueue_script( 'custom-adm', get_stylesheet_directory_uri() . '/js/custom-admin.js', array('jquery'), '', true );
        wp_enqueue_style( 'custom-adm', get_stylesheet_directory_uri() . '/css/theme-admin.css', array(), '' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

function typical_title() { // функция вывода тайтла
    global $page, $paged; // переменные пагинации должны быть глобальными
    wp_title('|', true, 'right'); // вывод стандартного заголовка с разделителем "|"
    bloginfo('name'); // вывод названия сайта
    $site_description = get_bloginfo('description', 'display'); // получаем описание сайта
    if ($site_description && (is_home() || is_front_page())) //если описание сайта есть и мы на главной
        echo " | $site_description"; // выводим описание сайта с "|" разделителем
    if ($paged >= 2 || $page >= 2) // если пагинация была использована
        echo ' | '.sprintf(__( 'Страница %s'), max($paged, $page)); // покажем номер страницы с "|" разделителем
}


## Проверяет есть ли указанная роль в ролях текущего/указанного пользователя
## $roles строка/массив - название роли которую нужно проверить у текущего пользователя
function is_user_role_in( $roles, $user = false ){
    if( ! $user )           $user = wp_get_current_user();
    if( is_numeric($user) ) $user = get_userdata( $user );

    if( empty($user->ID) )
        return false;

    foreach( (array) $roles as $role )
        if( isset($user->caps[ $role ]) || in_array($role, $user->roles) )
            return true;

    return false;
}


function get_rows_in_col( $count ) {

    if( $count%2 == 0 ) {
        return ($count/2);
    } else {
        return ($count/2+1);
    }
}

