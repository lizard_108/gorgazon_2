<div class="h2 no-caps">Похожие товары</div>
<div class="services-list-2">
    <?for ($i=0; $i < 3; $i++) { ?>
        <div class="services-list-2--item">
            <div class="services-list-2--img">
                <img src="<?=get_stylesheet_directory_uri()?>/images/other/demo-2.png" alt="">
            </div>
            <div class="services-list-2--text">
                <div class='h3'><?php the_title() ?></div>
                <p><?php the_field('description_short') ?></p>
                <div class="flex">
                    <?include "inc/stars.php"?>
                    <div class="services-list-2--reviews">66 отзывов</div>
                </div>
                <div class="services-list-2--price">104 руб./рулон</div>
                <div class="services-list-2--price-2">116 руб./м2</div>
                <div class="group">
                    <div class="cell size-50">Цены указаны с НДС 20%</div>
                    <div class="cell size-50"><div class="show-more-link"><a href="">Подробнее</a></div></div>
                </div>
            </div>
        </div>
    <?}?>
</div>