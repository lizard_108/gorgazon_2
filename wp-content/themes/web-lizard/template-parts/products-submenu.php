<?php

    $menu_title = get_field( 'extend_menu_title' );
    $menu = get_field( 'extend_menu' );

    $menu_html = '';

    if ( is_numeric( $menu ) ) {
        $menu_html = wp_nav_menu( [
            'menu'          => $menu,
            'container'     => '',
            'menu_class'    => 'services-submenu--menu',
            'echo'          => false
        ] );
    }

    if( $menu_html ){
        echo "<div class='services-submenu'>
                    <div class='services-submenu--title'>{$menu_title}</div>
                    {$menu_html}
                </div>";
    } else {
        if( is_user_role_in(['administrator','editor']) ){
            echo "<div class='inserts'>МЕНЮ РАЗДЕЛА</div>";
        }
    }
?>

