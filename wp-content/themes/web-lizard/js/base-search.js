$(document).ready(function() {
    var minlen = 3; // минимальная длина слова
    var paddingtop = 30; // отступ сверху при прокрутке
    var scrollspeed = 200; // время прокрутки
    var keyint = 1000; // интервал между нажатиями клавиш
    var term = '';
    var n = 0;
    var time_keyup = 0;
    var time_search = 0;

    $('body').delegate('#spgo', 'click', function() {
        $('body,html').animate({
            scrollTop: $('span.highlight:first').offset().top - paddingtop
        }, scrollspeed); // переход к первому фрагменту
    });

    function dosearch() {
        term = $('#spterm').val();
        $('span.highlight').each(function() { //удаляем старую подсветку
            $(this).after($(this).html()).remove();
        });
        var t = '';
        $('div#content').each(function() { // в селекторе задаем область поиска
            $(this).html($(this).html().replace(new RegExp(term, 'ig'), '<span class="highlight">$&</span>')); // выделяем найденные фрагменты
            n = $('span.highlight').length; // количество найденных фрагментов


            // console.log('n = ' + n);
        });
        $(".article-list li").addClass('search-passive');
        $(".search-reset").show();
        $("#content .h2").hide();

            $("#content .delivery-list").hide();


        $("#content .highlight").each(function(index, el) {

            $(this).closest('.delivery-list').show('');


            $(this).closest('li').removeClass('search-passive');
            $(this).closest('li').addClass('search-active');
        });
    }

    $('#spterm').keyup(function() {
        var d1 = new Date();
        time_keyup = d1.getTime();
        if ($('#spterm').val() != term) // проверяем, изменилась ли строка
            if ($('#spterm').val().length >= minlen) { // проверяем длину строки
                setTimeout(function() { // ждем следующего нажатия
                    var d2 = new Date();
                    time_search = d2.getTime();
                    if (time_search - time_keyup >= keyint) // проверяем интервал между нажатиями
                        dosearch(); // если все в порядке, приступаем к поиску
                }, keyint);
            }
        else
            $('#spresult').html('&nbsp'); // если строка короткая, убираем текст из DIVа с результатом 
    });

    $(".js-search-reset").click(function(event) {
    	/* Act on the event */
    	$(".article-list li").removeClass('search-passive');
    	$(".article-list li").removeClass('search-active');
    	$(".search-reset").hide();
    	$("#spterm").val("");
        $("#content .h2").show();
        $("#content .delivery-list").show();
    	return false;
    });

    if (window.location.hash != "") // бонус
    {
        var t = window.location.hash.substr(1, 50); // вырезаем текст
        $('#spterm').val(t).keyup(); // вставляем его в форму поиска
        $('#spgo').click(); // переходим к первому фрагменту
    }
});