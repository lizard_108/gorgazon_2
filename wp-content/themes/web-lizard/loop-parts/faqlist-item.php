<?php

    $cats = [];
    $post_cats = get_the_category();

    foreach ( $post_cats as $cat ) {
        if ( ! in_array( $cat->slug, $cats ) ) {
            $cats[] = $cat->slug;
        }
    }

    $cats_str = implode(',', $cats );
    if( $cats_str ) {
        $add_data = "data-cats='{$cats_str}' ";
    }

?>

<div <?=$add_data?> class="faq-list--item visible">
    <div class="question"><span><?php the_content() ?></span></div>
    <div class="answer">
        <?php the_field('faq_answer');?>
    </div>
</div>
