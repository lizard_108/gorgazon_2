<!-- /// -->
<div class="h2 reviews-title">Отзывы <span>о укладке рулонного газона</span></div>
<div class="reviews">
    <div class="reviews-total">
        <div class="number-rating">4.5</div>
        <div class="big-stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/big-stars.png" alt=""></div>
    </div>
    <div class="total-reviews-count">66 отзывов</div>
    <div class="reviews--list reviews-list-in-categoty">
        <?php
        for ($i=0; $i < 3; $i++) {
            get_template_part( 'loop-parts/review' );
        }?>
    </div>
    <div class="show-more-button">
        <a href="">Показать больше</a>
    </div>
</div>
<!-- /// -->