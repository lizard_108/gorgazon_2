WebP Express 0.17.3. Conversion triggered with the conversion script (wod/webp-realizer.php), 2020-02-27 16:13:21

*WebP Convert 2.3.0*  ignited.
- PHP version: 7.2.24-0ubuntu0.18.04.3
- Server software: Apache/2.4.29 (Ubuntu) mod_fcgid/2.3.9 OpenSSL/1.1.1

Stack converter ignited
Destination folder does not exist. Creating folder: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/web-lizard/images/other

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/web-lizard/images/other/header-banner.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/web-lizard/images/other/header-banner.jpg.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "none"
- near-lossless: 60
- quality: "auto"
------------


*Trying: gd* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/web-lizard/images/other/header-banner.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/web-lizard/images/other/header-banner.jpg.webp
- default-quality: 70
- log-call-arguments: true
- max-quality: 80
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- encoding
- metadata
- near-lossless
- skip-pngs
------------

GD Version: 2.2.5
image is true color
Quality of source is 99. This is higher than max-quality, so using max-quality instead (80)
gd succeeded :)

Converted image in 160 ms, reducing file size with 87% (went from 119 kb to 16 kb)
