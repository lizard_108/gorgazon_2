<?php
    $curid = get_the_ID();
    $ancestors = array_reverse( get_post_ancestors( $curid ) );
?>

<div class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
    <div class="breadcrumbs--back"></div>
	<ul>
        <?php
            $bc = '';
            $bc .= "<li itemprop='itemListElement' itemscope='' itemtype='http://schema.org/ListItem'><a href='" . home_url() . "' itemprop='item'><span itemprop='name'>Главная</span></a><meta itemprop='position' content='1'></li>";
            $s = 2;
            if ( get_post_type($curid) == 'post' ) {
                $bc .= "<li itemprop='itemListElement' itemscope='' itemtype='http://schema.org/ListItem'><a href='" . home_url() . "' itemprop='item'><span itemprop='name'>Блог</span></a><meta itemprop='position' content='{$s}'></li>";
                $s++;
            }
            foreach ( $ancestors as $anc ) {
                $skip_arr = [380]; // страницы с данными айди исключаются из цепочки
                if( in_array( $anc, $skip_arr ) ) {
                    continue;
                }
                $bc .= "<li itemprop='itemListElement' itemscope='' itemtype='http://schema.org/ListItem'><a href='" . get_permalink( $anc ) . "' itemprop='item'><span itemprop='name'>" . wp_strip_all_tags( get_the_title( $anc ) ) . "</span></a><meta itemprop='position' content='".$s."'></li>";
                $s++;
            }

            $bc .= "<li itemprop='itemListElement' itemscope='' itemtype='http://schema.org/ListItem'><a href='" . get_permalink( $curid ) . "' itemprop='item'><span itemprop='name'>" . wp_strip_all_tags( get_the_title( $curid ) ) . "</span></a><meta itemprop='position' content='".$s."'></li>";
            echo $bc;
        ?>
	</ul>
</div>

