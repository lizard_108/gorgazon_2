<?
// actions loop

$act_args = array(
    'post_type' => 'page',
    'posts_per_page' => '-1',
    'post_status' => 'publish',
    'order' => 'ASC',
    'post_parent' => get_the_ID(),
    'meta_query' => array(
        array(
            'key'     => 'action_date_expire',
            'value'   => date( "Y-m-d H:i:s" ),
            'compare' => '>',
            'type'    => 'DATETIME',
        ),
    ),
);

$actions = new WP_Query($act_args);

if ( $actions->have_posts() ) : while ( $actions->have_posts() ) : $actions->the_post();

    get_template_part('loop-parts/action', 'item');

endwhile;
    wp_reset_postdata();
else: ?>

    <p><?php _e('Сейчас нет действующих акций, но мы работаем над этим :)'); ?></p>

<?php endif;