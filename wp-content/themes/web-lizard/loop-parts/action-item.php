<div class="actions-list--item">
    <a href="<?php the_permalink() ?>">
        <img src="<?php the_post_thumbnail_url();?>">
        <small><?php echo "действует до: " . get_field('action_date_expire' ) ?></small>
        <div class="h3"><?php the_field('action_short_descr' ) ?></div>
    </a>
</div>