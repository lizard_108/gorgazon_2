<?php
/**
 * Template Name: Доставка - единичная страница
 * @package WordPress
 */
get_header(); ?>

    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>
            <div class="main-content content-wrap">
                <div class="logo-header">
                    <div class="group">
                        <div class="cell size-66">
                            <div class="h2 no-caps"><?php the_title()?></div>
                        </div>
                        <div class="cell size-33">
                            <img src="<?php the_post_thumbnail_url();?>">
                        </div>
                    </div>
                </div>

                <?php get_template_part('template-parts/page', 'intro' );?>
                <?php get_template_part('template-parts/insert1' );?>
                <div class='space-40'></div>

                <?php get_template_part('template-parts/content2' );?>
                <div class='space'></div>
                <?php get_template_part('template-parts/insert2' );?>

            </div>
        </div>

        <?php get_template_part('template-parts/insert3' );?>

    </main>

<?php get_footer();