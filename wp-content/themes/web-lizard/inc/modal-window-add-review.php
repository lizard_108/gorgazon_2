<div class="modal-window modal-window--add-review add-comment--fields-place">
	<div class="close"></div>
	<form id="add-review">
		<div class="modal--title">
			Добавление отзыва
		</div>
		<div class="modal--subtitle">Отзыв о услуге: Укладка рулонного газона на готовое основание </div>
		<div class="group">
			<div class="cell size-33">
				<span>Рейтинг</span>
			</div>
			<div class="cell size-66">
				<div class="module-stars active" data-rating="4">
					<div class="module-stars--control"></div>
				</div>
			</div>
		</div>
		<div class="group">
			<div class="cell size-33">
				<span>Комментарий</span>
			</div>
			<div class="cell size-66">
				<div class="add-comment--textarea">
					<textarea name="">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки</textarea>
					<a href="#" class="comments--attach"></a>
				</div>
			</div>
		</div>
		<div class="modal--subtitle">Контактная информация </div>
		<div class="group">
			<div class="cell size-33"><span>Ваше имя</span></div>
			<div class="cell size-66">
				<label class="static-input">
					<input type="text" value="" placeholder="Например: Андрей">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-33"><span>Электронная почта</span></div>
			<div class="cell size-66">
				<label class="static-input">
					<input type="text" value="" placeholder="Введите вашу почту">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-33"><span>или войдите через соц. сеть</span></div>
			<div class="cell size-66">
				<div class="social">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-facebook.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-vk.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-instagram.svg"></a>
				</div>
			</div>
		</div>
		<div class="group">
			<div class="cell size-33">
				<div class="personal-info">
					Нажимая на кнопку оставить комментарий, вы даете согласие на обработку <b>персональных данных</b>
				</div>				
			</div>
			<div class="cell size-66">
				<div class="btn-green">Оставить комментарий</div>
			</div>
		</div>
	</form>
</div>