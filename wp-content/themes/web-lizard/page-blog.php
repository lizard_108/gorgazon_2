<?php
/**
 * Template Name: страница "Блог 1"
 * Template Post Type: post, page
 * @package WordPress
 */
get_header(); // подключаем header.php ?>


    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>
            <?php get_template_part('template-parts/insert1' );?>
            <div class="main-content">
                <div class="h1 no-caps"><?php the_title()?></div>
                <?php get_template_part('template-parts/blog-tabs' );?>
                <div class="space-40"></div>
                <div class="blog-list">
                    <?php get_template_part('loops/blog' );?>
                </div>
                <?php //get_template_part('template-parts/pagination' );?>

            </div>
            <?php get_template_part('template-parts/insert2' );?>
        </div>

        <?php get_template_part('template-parts/insert3' );?>
        <?php get_template_part('template-parts/content' );?>
    </main>

<? get_footer(); // подключаем footer.php ?>