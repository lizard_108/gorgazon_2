<?php

    $content = apply_filters('the_content', get_post( get_the_ID() )->post_content );
    if ( $content ) : $parts = get_extended( $content );
?>

    <div class="content-container">
        <div class="main-content content-wrap">
            <div class="content-anons">
                <?php echo $parts['main'] ?>
            </div>

            <?php if ( $parts['extended'] ) : ?>
                <div class="article-determined-block--hidden">
                    <?php echo $parts['extended'] ?>
                </div>
                <div class="article-determined-block--more js-article-determined-block--open-seo">
                    <a href="">Показать больше</a>
                </div>
            <?php endif; ?>
        </div>
    </div>

<?php endif;