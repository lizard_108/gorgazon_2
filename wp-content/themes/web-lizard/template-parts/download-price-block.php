<?php if ( get_field( 'price_file' ) ) : ?>

    <div class="download-price-block">
        <a href="<?php the_field( 'price_file' );?>" class="download-price-block--link">
            <div>
                <b>Скачать прайс лист</b>
                <br>
                <small>Действует с <?php the_field( 'price_date' );?></small>
            </div>
        </a>
        <div class="download-price-block--info"><?php the_field( 'price_comment' );?></div>
    </div>

<?php endif;