<?php
/**
 * Template Name: Блог - список новостей
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<div class="main-content">
			<div class="index-banner">
				<img src="<?=get_stylesheet_directory_uri()?>/images/other/index-banner.png">
			</div>
            <div class="space"></div>

			<div class="h2 h2-link">Продукция <a href="<?php echo home_url().'/produktsiya/';?>">перейти в каталог</a></div>
            
            <div class="mainpage-anomaly">
                <p>У нас Вы можете купить <strong>рулонный газон</strong>, а также заказать устройство <strong>посевного газона</strong>. А еще мы предлагаем недорогую и оперативную <strong>доставку газона</strong> по европейской части России.</p>
            </div>

			<div class="services-list">

            <?
                $prod_args = array(
                    'post_type'         => 'page',
                    'posts_per_page'    => '3',
                    'post_status'       => 'publish',
                    'order'             => 'ASC',
                    'post_parent'       => 152,
                );

                $products = new WP_Query( $prod_args );

                if ( $products->have_posts() ) : while ( $products->have_posts() ) : $products->the_post();
                    get_template_part('loop-parts/servprod', 'item' );
                endwhile; else: ?>

                    <p><?php _e('Здесь ничего нет.'); ?></p>

                <?php endif; ?>


			</div>
			<div class="show-mobile all-list-link"><a href="">Все услуги</a></div>

            <div class="space"></div>

			<div class="h2 h2-link">Услуги <a href="<?php echo home_url().'/uslugi/';?>">все услуги</a></div>

			<div class="services-list">
				<?
                $serv_args = array(
                    'post_type'         => 'page',
                    'posts_per_page'    => '3',
                    'post_status'       => 'publish',
                    'order'             => 'ASC',
                    'post_parent'       => 38,
                );

                $services = new WP_Query( $serv_args );

                if ( $services->have_posts() ) : while ( $services->have_posts() ) : $services->the_post();
                    get_template_part('loop-parts/servprod', 'item' );
                endwhile; wp_reset_postdata();  else: ?>

                    <p><?php _e('Здесь ничего нет.'); ?></p>

                <?php endif; ?>
			</div>

            <div class="space"></div>
            <?php

                get_template_part('template-parts/projects', 'slider');
            ?>
            <div class="space"></div>
            <?

                get_template_part('template-parts/insert1' ) ?>

			<div class="space"></div>

            <?php  get_template_part('template-parts/insert2' ) ?>
            <div class="space"></div>

            <?php  get_template_part('template-parts/reviews' ) ?>


			<div class="space"></div>

            <div class="h2 h2-link">Блог <a href="<?php echo home_url() . '/blog/'?>">все статьи</a></div>
            <div class="blog-list">
                <?php get_template_part('loops/blog', '2items' ) ?>
            </div>

            <?php // get_template_part('template-parts/blog' ) ?>



		</div>
	</div>
    <?php get_template_part('template-parts/insert3' ) ?>

	<?php
        wp_reset_postdata();
        get_template_part('template-parts/content' );
    ?>

</main>

<? get_footer(); // подключаем footer.php ?>