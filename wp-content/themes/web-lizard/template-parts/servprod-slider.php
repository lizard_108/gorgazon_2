<?php


    $img_arr = [];

    $img1 = get_field('ext_img1' );
    $img2 = get_field('ext_img2' );
    $img3 = get_field('ext_img3' );
    $img4 = get_field('ext_img4' );
    $img5 = get_field('ext_img5' );

    if($img1){
        $img_arr[] = $img1['ID'];
    }

    if($img2){
        $img_arr[] = $img2['ID'];
    }

    if($img3){
        $img_arr[] = $img3['ID'];
    }

    if($img4){
        $img_arr[] = $img4['ID'];
    }

    if($img5){
        $img_arr[] = $img5['ID'];
    }

    if ( count( $img_arr ) > 1 ) {
        $slider_list = '';
        $sub_slider_list = '';
        $i = 0;
        foreach ( $img_arr as $id ) {

            $imgcode = kama_thumb_img( array(
                'src' => wp_get_attachment_image_url( $id, 'full' ),
                'w' => 435,
                'h' => 350,
            ) );
            $slider_list .= "<div class='item'>{$imgcode}</div>";

            $subimgcode = kama_thumb_img( array(
                'src' => wp_get_attachment_image_url( $id, 'full' ),
                'w' => 70,
                'h' => 70,
            ) );
            if( !$i++ ){
                $cur = ' current';
            }else{
                $cur = '';
            }
            $sub_slider_list .= "<div class='item{$cur}'>{$subimgcode}</div>";
        }
        ?>

        <div class="services-product--slider">
            <div class="js-services-product--slider">
                <?php echo $slider_list ?>
            </div>
            <div class="services-product--slider--subslider">
                <?php echo $sub_slider_list ?>
            </div>
        </div>

        <?php
    } elseif ( count( $img_arr ) > 0  ) { ?>

        <div class="services-product--image">
            <?php

                echo kama_thumb_img( array(
                        'src' => wp_get_attachment_image_url($img_arr[0], 'full'),
                        'w' => 435,
                        'h' => 350,
                    ) );
            ?>
        </div>

    <?php }