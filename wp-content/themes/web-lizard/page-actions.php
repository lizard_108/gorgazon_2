<?php
/**
 * Template Name: страница "Все акции"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?>

    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>
            <div class="main-content">
                <div class="h1">Акции</div>
                <div class="actions-list">
                    <?php get_template_part('loops/actions'); ?>
                </div>
                <!--div class="blog-controls">
                    <div class="blog-controls--show-more">Загрузить еще</div>
                    <ul class="blog-controls--pagination">
                        <li>1</li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                    </ul>
                </div-->
                <div class="space"></div>
                <div class="h2 h2-link">Блог <a href="<?php echo get_permalink(247) ?>">все статьи</a></div>

                <div class="blog-list adapt-overflow">
                    <?php get_template_part('loops/blog') ?>
                </div>
            </div>
        </div>

        <?php get_template_part("inc/question-form") ?>
        <?php get_template_part('template-parts/content' ) ?>
    </main>

<? get_footer(); // подключаем footer.php ?>