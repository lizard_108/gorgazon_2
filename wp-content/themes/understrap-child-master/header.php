<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style.css">
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/reject/reject.css" media="all" />
		<script type="text/javascript" src="<?=get_stylesheet_directory_uri()?>/reject/reject.min.js"></script>
	<![endif]-->
	<title><?php typical_title();  ?></title>
	<?php wp_head(); ?>


	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	

	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/css/style.min.css?<?=time();?>">
	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.transitions.css">
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/jquery-1.12.4.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/sliders.js"></script>
</head>
<body>
	<div class="darkness"></div>
	<div class="wrapper">
		<div class="main-container">
			<header>
				<div class="main-logo">
					<a href="/">
						<img src="<?=get_stylesheet_directory_uri()?>/images/icons/main-logo.png" alt="Горгазон">
						<br>
						<span>Горгазон</span>
					</a>
				</div>
				<div class="header--phone">
					<div class="current">8 (499) 136-78-86</div>
					<div class="get-callback">
						<a href="">Заказать обратный звонок</a>
					</div>
				</div>
				<div class="header--actions-discounts">
					<a href="">Акции и скидки</a>
				</div>
				<nav>
					<ul class="header--menu">
						<li><a href="">Продукция</a></li>
						<li><a href="">Услуги</a></li>
						<li><a href="">О компании</a></li>
						<li><a href="">Проекты</a></li>
						<li><a href="">Отзывы</a></li>
						<li><a href="">Блог</a></li>
						<li><a href="">Контакты</a></li>
					</ul>
					<ul class="header--menu part-2">
						<li class="has-child">
							<a href="">Покупателю</a>
							<ul>
								<li><a href="">Вопрос ответ</a></li>
								<li><a href="">База знаний</a></li>
								<li><a href="">Доставка</a></li>
								<li><a href="">Оплата</a></li>
								<li><a href="">Гарантия</a></li>
							</ul>
						</li>
						<li><a href="">Сотрудничество</a></li>
						<li><a href="">Вакансии</a></li>
					</ul>
				</nav>
				<div class="header--email">
					sales@gorgazon.ru
				</div>
				<div class="header-banner">
					<a href="/"><img src="<?=get_stylesheet_directory_uri()?>/images/other/header-banner.jpg"></a>
				</div>
			</header>
			<div class="main">