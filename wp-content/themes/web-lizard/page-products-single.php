<?php
/**
 * Template Name: страница "Продукция - единичная страница"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>

        <?php get_template_part('template-parts/products', 'submenu');?>
		
		<div class="main-content" itemscope itemtype="http://schema.org/Product">
			<div class="h1" itemprop="name"><?php the_title()?></div>
            <?php get_template_part('template-parts/page', 'intro') ?>

            <?php get_template_part('template-parts/product', 'single');?>

            <?php get_template_part('template-parts/snoska'); ?>

            <?php get_template_part('template-parts/servprod', 'tabs' ); ?>

			<div class="space"></div>

            <?php get_template_part('template-parts/insert1');?>
			<div class="space"></div>

            <?php get_template_part('template-parts/insert2');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert3');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert4');?>
            <div class="space"></div>

			<?include "inc/reviews-inner.php"?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert5');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert6');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert7');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/similar-goods');?>
            <div class="space"></div>

            <?php get_template_part('template-parts/insert8');?>

		</div>
	</div>

    <?php get_template_part('template-parts/insert9');?>
	<?php get_template_part('template-parts/content' );?>
</main>

<?php get_footer();