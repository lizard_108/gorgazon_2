<?php

    require_once get_stylesheet_directory() . '/libs/PHPMailer/PHPMailer.php';
    require_once get_stylesheet_directory() . '/libs/PHPMailer/SMTP.php';


    function send_smtp_mail( $subj, $html ) {

        // Создаем письмо
        $mail = new PHPMailer();

        $mail->isSMTP();                   // Отправка через SMTP
        $mail->Host = 'smtp.yandex.ru';  // Адрес SMTP сервера
        $mail->SMTPAuth = true;          // Enable SMTP authentication
        $mail->Username = 'zakaz@gorgazon.ru';       // ваше имя пользователя (без домена и @)
        $mail->Password = 'GazonGazon++';    // ваш пароль
        $mail->SMTPSecure = 'ssl';         // шифрование ssl
        $mail->Port = 465;               // порт подключения

        $mail->setFrom('zakaz@gorgazon.ru', 'Cайт gorgazon.ru');    // от кого
        $mail->addAddress('site@gorgazon.ru', 'менеджеру по продажам'); // кому

        $mail->Subject = $subj;
        $mail->msgHTML( $html );
        // Отправляем
        if ($mail->send()) {
            return 1;
        } else {
            return 0;
        }
    }


    add_action( 'wp_ajax_send_callback_form', 'send_callback_form' );
    add_action( 'wp_ajax_nopriv_send_callback_form', 'send_callback_form' );

    function send_callback_form() {

        $data = json_decode( filter_input(INPUT_POST, 'formData' ) );

        $subj = "Заявка с сайта gorgazon.ru";
        $html = "
            <html>
                <body>
                    <h1>Заявка на обратный звонок</h1>
                    <p>Добрый день!</p>
                    <p>Прошу перезвонить мне по номеру {$data->phone}.</p>
                    <p>С уважением, {$data->name}</p>
                </body>
            </html>";

        if(send_smtp_mail($subj, $html)) {
            echo 'success';
        }

        wp_die();
    }

    add_action( 'wp_ajax_send_gazon_order_form', 'send_gazon_order_form' );
    add_action( 'wp_ajax_nopriv_send_gazon_order_form', 'send_gazon_order_form' );

    function send_gazon_order_form() {

        $data = json_decode( filter_input(INPUT_POST, 'formData' ) );

        $subj = "Заявка с сайта gorgazon.ru";
        if($data->comment){
            $comment = "<br><p>Дополнительная информация: {$data->comment}</p>";
        }

        if ( $data->url ) {
            $gazon = "газон <a href='" . home_url() . $data->url . "'>" . get_page_by_path($data->url)->post_title . "</a>";
        } else {
            $gazon = "газон";
        }


        $html = "
            <html>
                <body>
                    <h1>Заказ газона</h1>
                    <p>Добрый день!</p>
                    <p>Меня интересует {$gazon} для участка площадью {$data->square} кв.м.</p>
                    <p>Прошу перезвонить мне по номеру {$data->phone}.</p>
                    <p>С уважением, {$data->name}</p>
                    {$comment}
                </body>
            </html>";

        if(send_smtp_mail($subj, $html)) {
            echo 'success';
        }

        wp_die();
    }

    add_action( 'wp_ajax_send_plitka_order_form', 'send_plitka_order_form' );
    add_action( 'wp_ajax_nopriv_send_plitka_order_form', 'send_plitka_order_form' );

    function send_plitka_order_form() {

        $data = json_decode( filter_input(INPUT_POST, 'formData' ) );

        $subj = "Заявка с сайта gorgazon.ru";
        if($data->comment){
            $comment = "<br><p>Дополнительная информация: {$data->comment}</p>";
        }

        if ( $data->url ) {
            $tovar = "плитка <a href='" . home_url() . $data->url . "'>" . get_page_by_path($data->url)->post_title . "</a>";
        } else {
            $tovar = "плитка";
        }


        $html = "
            <html>
                <body>
                    <h1>Заказ плитки</h1>
                    <p>Добрый день!</p>
                    <p>Меня интересует {$tovar} для участка площадью {$data->square} кв.м.</p>
                    <p>Прошу перезвонить мне по номеру {$data->phone}.</p>
                    <p>С уважением, {$data->name}</p>
                    {$comment}
                </body>
            </html>";

        if(send_smtp_mail($subj, $html)) {
            echo 'success';
        }

        wp_die();
    }
