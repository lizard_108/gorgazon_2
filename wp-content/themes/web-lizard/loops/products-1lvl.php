<?
// products 1 level page main part

    $prod_args = array(
        'post_type' => 'page',
        'posts_per_page' => '-1',
        'post_status' => 'publish',
        'order' => 'ASC',
        'post_parent' => get_the_ID(),
    );

    $products = new WP_Query($prod_args);

    if ( $products->have_posts() ) : while ( $products->have_posts() ) : $products->the_post();

        get_template_part('loop-parts/servprod', 'item');

    endwhile; wp_reset_postdata();
    else: ?>

        <p><?php _e('Здесь ничего нет.'); ?></p>

    <?php endif;