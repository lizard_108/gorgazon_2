<div class="modal-window modal-window--order add-comment--fields-place">
	<div class="close"></div>
	<form id="callback-order-gazon-form">
		<div class="callback--subtitle">Заказать газон</div>
		<div class="callback--sub-subtitle">Для отправки заказа, пожалуйста, заполните необходимые поля:</div>
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div class="label">Ваш телефон*</div>
					<input id='gazon-order-phone' type="text" placeholder="+7 (___) ___-__-__" class="input--phone">
				</label>
			</div>
		</div>
		<!--div class="group">
			<div class="cell size-100">
				<select name="">
					<option value="">Сорт рулонного газона*</option>
					<option value="">Сорт рулонного газона*</option>
					<option value="">Сорт рулонного газона*</option>
				</select>
			</div>
		</div -->
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div class="label">Площадь газона (в кв.м.)*</div>
					<input id='gazon-order-square' type="text" placeholder="">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div class="label">Ваше имя*</div>
					<input id='gazon-order-name' type="text" placeholder="">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<div class="textarea-input">
					<textarea id='gazon-order-comment' name="" placeholder="Комментарий"></textarea>
				</div>
			</div>
		</div>
		<div class="group callback--submit-place">
			<div class="cell size-100">
				<div id='gazon-order-submit' class="btn-green">Оформить заказ</div>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<small>Нажимая на кнопку отправить, вы даете согласие на обработку <a href="">персональных данных</a></small>
			</div>
		</div>
	</form>
	<div class="modal-window--message-after-send">
		<p>Спасибо!</p>
		<p>Наши менеджеры свяжутся с Вами в ближайшее время</p>
		<div class="btn-green js-close-modal-windows">Ок</div>
	</div>
</div>