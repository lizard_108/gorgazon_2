<?php
/**
 * Template Name: Тротуарная плитка - единичная страница
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>

        <?php get_template_part('template-parts/products', 'submenu');?>
		
		<div class="main-content" itemscope itemtype="http://schema.org/Product">
			<div class="h1" itemprop="name"><?php the_title()?></div>
            <?php get_template_part('template-parts/page', 'intro') ?>

            <?php get_template_part('template-parts/product', 'plitka');?>

            <?php get_template_part('template-parts/snoska'); ?>

            <?php get_template_part('template-parts/servprod', 'tabs' ); ?>

            <?php get_template_part('template-parts/insert1');?>
            <?php get_template_part('template-parts/insert2');?>
            <?php get_template_part('template-parts/insert3');?>
            <?php get_template_part('template-parts/insert4');?>

			<?include "inc/reviews-inner.php"?>

            <?php get_template_part('template-parts/insert5');?>
            <?php get_template_part('template-parts/insert6');?>
            <?php get_template_part('template-parts/insert7');?>
            <?php get_template_part('template-parts/similar-goods');?>
            <?php get_template_part('template-parts/insert8');?>

		</div>
	</div>

    <?php get_template_part('template-parts/insert9');?>
	<?php get_template_part('template-parts/content' );?>
</main>

<?php get_footer();