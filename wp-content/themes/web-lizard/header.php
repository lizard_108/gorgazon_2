<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style.css">
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/reject/reject.css" media="all" />
		<script type="text/javascript" src="<?=get_stylesheet_directory_uri()?>/reject/reject.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>


	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	

	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/css/style.css?<?=time();?>">
	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.transitions.css">
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/jquery-1.12.4.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/jquery.formstyler.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/jquery.mask.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/sliders.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/custom.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/calc.js"></script>
</head>
<body>
	<div class="window-blur"></div>
	<?
	include 'inc/modal-window-calc.php';
	include 'inc/modal-window-add-review.php';
	include 'inc/modal-window-callback.php';
	include 'inc/modal-window-order.php';
	include 'inc/modal-window-order-plitka.php';
	?>
	<div class="wrapper">
		<a href="tel:84991367886" class="adapt--mobile-button"></a>
		<div class="adapt-header">
			<div class="adapt-header--wr">
				<div class="adapt-header--sandwich js-open-sandwich"></div>
				<div class="adapt-header--logo"><span>ГОРГАЗОН</span></div>
				<div class="adapt-header--buttons">
					<a href="" class="button-search js-open-adapt-search"></a>
					<a href="" class="button-share js-open-adapt-share"></a>
					<a href="" class="button-calc js-open-calc"></a>
				</div>
			</div>
			<div class="sandwich-open">
				<div class="top-level">
					<div class="adapt-header--logo"></div>
					<div class="adapt-header--close js-close-sandwich"></div>
				</div>
				<div class="menu-level">
					<div class="header--actions-discounts">
						<a href="<?php echo get_permalink(408) ?>">Акции и скидки</a>
					</div>
					<nav>
                        <?php
                        wp_nav_menu( [
                            'menu'  => 'MainLeft',
                            'container' => '',
                            'menu_class' => 'header--menu',
                        ] );

                        wp_nav_menu( [
                            'menu'  => 'MainLeftExt',
                            'container' => '',
                            'menu_class' => 'header--menu, part-2'
                        ] );
                        ?>
					</nav>
					<div class="adapt-header--contacts">
						<div class="adapt-header--phone">
							<div><strong>8 (499) 136-78-86</strong></div>
							<div>Москва</div>
						</div>
						<div class="adapt-header--phone">
							<div><strong>8 (800) 770-76-86</strong></div>
							<div>Россия</div>
						</div>
						<div class="text-1">ежедневно c 9:00 до 23:00 </div>
						<div class="text-2">sales@gorgazon.ru</div>
					</div>
				</div>
			</div>
			<div class="adapt-search-open">
				<div class="search">
					<form action="">
						<input type="text" placeholder="Поиск по сайту" value="" name="search" class="search--input">
					</form>
				</div>
			</div>
			<div class="adapt-share-open">
				<div class="adapt-share-open--wr">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-facebook.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-vk.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-instagram.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-od.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-email.svg"></a>
				</div>
			</div>

		</div>
		<div class="main-container">
			<header itemscope itemtype="https://schema.org/Organization">
				<div class="main-logo" >
					<a href="<?php home_url() ?>" itemprop="url">
						<img src="<?=get_stylesheet_directory_uri()?>/images/svg/main-logo.svg" alt="Горгазон" itemprop="logo">
						<br>
						<span itemprop="name">Горгазон</span>
					</a>
				</div>
				<div class="header--phone">
					<div class="current">
						<div class="current-phone" itemprop="telephone">8 (499) 136-78-86</div>
						<div class="header--hidden-phones">
							<div class="header--hidden-phone has-arrow" itemprop="telephone">8 (499) 136-78-86</div>
							<div class="header--hidden-city" itemprop="contactPoint">Москва</div>
							<div class="header--hidden-phone" itemprop="telephone">8 (800) 770-76-86</div>
							<div class="header--hidden-city" itemprop="contactPoint">Россия</div>
							<div class="header--hidden-space"></div>
							<div class="header--hidden-city">ежедневно c 9:00 до 23:00</div>
							<div class="header--hidden-space"></div>
						</div>
					</div>
					<div class="get-callback">
						<a href="" class="js-open-callback-form">Заказать обратный звонок</a>
					</div>
				</div>
				<div class="header--actions-discounts">
                    <a href="<?php echo get_permalink(408) ?>">Акции и скидки</a>
				</div>
				<nav>
                    <?php
                    wp_nav_menu( [
                        'menu'  => 'MainLeft',
                        'container' => '',
                        'menu_class' => 'header--menu',
                    ] );

                    wp_nav_menu( [
                        'menu'  => 'MainLeftExt',
                        'container' => '',
                        'menu_class' => 'header--menu part-2'
                    ] );
                    ?>
				</nav>
				<div class="header--email" itemprop="email">
					sales@gorgazon.ru
				</div>
				<div class="header-banner">
					<a href="/"><img src="<?=get_stylesheet_directory_uri()?>/images/other/header-banner.jpg"></a>
				</div>
			</header>
			<div class="main">