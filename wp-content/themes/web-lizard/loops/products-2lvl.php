<?php
// products 2 level page main part

    $prod_args = array(
        'post_type'         => 'page',
        'posts_per_page'    => '-1',
        'post_status'       => 'publish',
        'order'             => 'ASC',
        'post_parent'       => get_the_ID(),
        'fields'            => 'ids'
    );

    $products = new WP_Query( $prod_args );

    foreach ( $products->posts as $prod_id) {

        echo "<div class='h2 no-caps'>" . get_the_title( $prod_id ) . "</div>" .
			  "<div class='services-list-2'>";

        $subprod_args = array(
            'post_type'         => 'page',
            'posts_per_page'    => '-1',
            'post_status'       => 'publish',
            'order'             => 'ASC',
            'post_parent'       => $prod_id,
        );

        $subproducts = new WP_Query( $subprod_args );
        if( $subproducts->post_count ) {
            while ( $subproducts->have_posts() ) {
                $subproducts->the_post();
                get_template_part('loop-parts/servprod', 'itembig');
            }
        } else {

            global $post;
            $post = get_post($prod_id);
            setup_postdata( $post );
            get_template_part('loop-parts/servprod', 'itembig' );

        }

        echo "</div>";
        echo '<div class="space-70"></div>';
    }

    wp_reset_postdata();