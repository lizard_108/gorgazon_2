<?php
/**
 * Template Name: Блог(запись) - единичная страница
 * @package WordPress
 * @subpackage your-clean-template
 */

get_header(); ?>

    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>
            <div class="main-content" itemscope itemtype="http://schema.org/Article">
                <div class="h1 no-caps" itemprop="headline"><?php the_title() ?></div>
                <ul class="blog-attributes on-blog-page">
                    <li>
                        <?php echo get_gg_rating_html(); ?>
                    </li>
                    <li><a href="">Укладка</a></li>
                    <li itemprop="datePublished" datetime="2019-01-22T16:20:30+00:00">12 декабря 2019</li>
                    <li>Горгазон</li>
                </ul>
                <div class="space"></div>

                <div class="content-wrap"><?php get_template_part('template-parts/content2' );?></div>

                <?include 'inc/share-block.php';?>
                <div class="space"></div>


                <?include 'inc/comments-block.php';?>
                <div class="space"></div>


                <div class="h2">Похожие статьи</div>
                <div class="blog-list">
                    <?php get_template_part('loops/blog' );?>
                </div>
            </div>
        </div>

        <?php get_template_part('template-parts/insert1' );?>

    </main>


<? get_footer();