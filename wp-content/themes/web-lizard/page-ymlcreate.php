<?php

get_header(); ?>
<main>
    <div class="content-container">
        <?include "inc/search.php"?>
        <?include "inc/breadcrumbs.php"?>

        <?php
            ini_set('error_reporting', E_ALL);
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);


            $args = array(
                'post_type'         => 'page',
                'posts_per_page'    => '-1',
                'post_status'       => 'publish',
                'fields'            => 'ids',
                'orderby'           => 'date',
                'order'             => 'DESC',
                'meta_query' => [
                    'relation' => 'OR',
                    [
                        'key' => 'page_type',
                        'value' => 'product'
                    ],
                ]
            );

            $results = new WP_Query( $args );

            $cats = [];
            foreach ( $results->posts as $res ) {
                if($res==152){
                    $cats[$res] = '';
                }else{
                    $par =  wp_get_post_parent_id( $res );
                    if(!in_array($par, $cats)){
                        $cats[$res] = $par;
                    }
                }
            }


            $endpages = [];
            foreach( $results->posts as $res ) {

                if( ! in_array( $res, $cats ) ){
                    $endp = [];
                    //$endp [ id, name, url, price_sale, price, cat_id, images[], descr ]
                    $endp['id'] = $res;
                    $endp['name'] = get_the_title($res);
                    $endp['url'] = get_permalink($res);

                    $price = get_field('extprice', $res );
                    if(!$price){
                        $price = get_field('price', $res );
                    }

                    if($price){
                        $endp['price'] = $price;
                    }

                    $endp['cat_id'] = wp_get_post_parent_id($res);

                    $img = get_the_post_thumbnail_url($res);
                    if($img){
                        $endp['images'][] = $img;
                    }

                    $img1 = get_field('ext_img1', $res );
                    if($img1){
                        $endp['images'][] = $img1['url'];
                    }

                    $img2 = get_field('ext_img2', $res );
                    if($img2){
                        $endp['images'][] = $img2['url'];
                    }

                    $img3 = get_field('ext_img3', $res );
                    if($img3){
                        $endp['images'][] = $img3['url'];
                    }

                    $img4 = get_field('ext_img4', $res );
                    if($img4){
                        $endp['images'][] = $img4['url'];
                    }

                    $img5 = get_field('ext_img5', $res );
                    if($img5){
                        $endp['images'][] = $img5['url'];
                    }


                    $descr = get_field('description_short_card', $res );
                    if ( ! $descr ) {
                        $descr = get_field('description_short', $res );
                    }
                    if($descr){
                        $endp['descr'] = $descr;
                    }


                    $endpages[] = $endp;
                }
            }


            $args = [
                'shopname' => 'Горгазон',
                'company' => 'ООО Агора',
                'categories' => $cats, // [ id => parent_id ]
                'products' => $endpages, // [][ id, name, url, price_sale, price, cat_id, images[], descr ]
            ];

       // echo "<pre>"; print_r($args); echo "</pre>";
            generateYML( $args );

            ?>

        </div>
    </div>

</main>

<? get_footer(); ?>

