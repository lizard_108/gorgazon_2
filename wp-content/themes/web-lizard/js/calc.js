$(document).ready(function() {
	$(".js-calc input, .js-calc select").change(function(event) {
		/* Act on the event */
		calculation();
	});

	function calculation () {
		var area = $(".js-calc input[name=area]").val();
		var obrez = $(".js-calc input[name=obrez]:checked").val();
		var sort = $(".js-calc select[name=sort] option:selected").val();
		var stoim = $(".js-calc input[name=stoim]").val();
		var col_rulons = $(".js-calc input[name=col_rulons]").val();
		var rueltivation = $(".js-calc input[name=rueltivation]").val();
		var ukladka = $(".js-calc input[name=ukladka]:checked").val();
		var naklad = $(".js-calc input[name=naklad]").val();
		var dostavka = $(".js-calc input[name=dostavka]:checked").val();
		// console.log(ukladka);
		$("#calc--area").html(area+" м2");
		if (obrez == "1") {
			$("#calc--zapas").html("есть обрез");
		} else {
			$("#calc--zapas").html("-");			
		}
		$("#calc--sort").html(sort);
		if (ukladka) {
			$("#calc--ukladka").html(ukladka + " руб.");
		} else {
			$("#calc--ukladka").html("-");			
		}

		if (dostavka == "300") {
			$("#calc--dostavka").html(dostavka + " руб.");
		} else {
			$("#calc--dostavka").html("-");
		}
	}
});