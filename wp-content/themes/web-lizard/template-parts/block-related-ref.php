<?php

    $ref_url = get_field('related_ref_url');
    $ref_text = get_field('related_ref_text');
    if ( $ref_url && $ref_text ) {
        echo "<div class='btn-transparent'><a href='{$ref_url}'>{$ref_text}</a></div>";
    }
