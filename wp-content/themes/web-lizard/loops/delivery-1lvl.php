<?php
// delivery 1 level

    $prod_args = array(
        'post_type'         => 'page',
        'posts_per_page'    => '-1',
        'post_status'       => 'publish',
        'order'             => 'ASC',
        'post_parent'       => get_the_ID(),
        'fields'            => 'ids'
    );

    $products = new WP_Query( $prod_args );

    foreach ( $products->posts as $prod_id) {

        $subprod_args = array(
            'post_type'         => 'page',
            'posts_per_page'    => '-1',
            'post_status'       => 'publish',
            'order'             => 'ASC',
            'post_parent'       => $prod_id,
        );

        $subproducts = new WP_Query( $subprod_args );

        if( $subproducts->post_count ) : ?>

            <div class='h2 no-caps'><?php echo get_the_title( $prod_id ) ?></div>

            <div class='delivery-list'>
                <div class='group'>

                    <div class='cell size-50'>
                        <ul class='article-list'>
                            <?php
                                $rows_count = get_rows_in_col( $subproducts->post_count );
                                $i = 0;
                                while ( $subproducts->have_posts() ) {
                                    if( ++$i > $rows_count ) {
                                        break;
                                    }
                                    $subproducts->the_post();
                                    get_template_part('loop-parts/delivery', 'item');
                                }
                            ?>
                        </ul>
                    </div>

                    <div class='cell size-50'>
                        <ul class='article-list'>
                            <?php
                                while ( $subproducts->have_posts() ) {
                                    $subproducts->the_post();
                                    get_template_part('loop-parts/delivery', 'item');
                                }
                            ?>
                        </ul>
                    </div>

                </div>
            </div>
        <?php endif;
    }

    wp_reset_postdata();
