
<div class="reviews--item">
    <div class="reviews--avatar" style="background-image: url()">
        <img src="<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg" alt="">
    </div>
    <div class="reviews--text-place">
        <div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
        <div class="reviews-total">
            <div class="number-rating">4.5</div>
            <div class="stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
        </div>
        <div class="reviews--people-info">
            <span>Игорь</span>
            <span>Организация</span>
        </div>
        <div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
    </div>
</div>