<?php
/**
 * Страница 404 ошибки (404.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // Подключаем header.php ?>
?>

	<main>
        <div class="content-container">
            <? include "inc/search.php" ?>

			<div class="space"></div>
            <div class="page-404">
            	<div class="page-404--title">404</div>
            	<div class="page-404--not-found">Страница не найдена</div>
            	<div class="page-404--btn"><a href="/" class="404--btn">На главную</a></div>
            </div>
			<div class="space"></div>
			<div class="space"></div>
			<div class="space"></div>

        </div>

    </main>
<?php get_footer(); // подключаем footer.php ?>