
<div class="services-product product-edition">

    <?php get_template_part( "template-parts/servprod-slider" )?>

    <div class="services-product--place">

        <!-- div class="flex services-product-rating"  itemprop="offers" itemscope itemtype="http://schema.org/Offer" -->

        <?php echo get_gg_rating_html() ?>

        <div class="services-product--description"><?php the_field('description_short_card') ?></div>

        

<?php
        $price2 = get_gg_extprice();
        $price = get_gg_price();

        if( $price2 ) {
            echo "<div class='services-product--price'>{$price2}</div>"
            . "<div class='services-product--price-2'>{$price}</div>";
            echo "<meta itemprop='price' content='{$price2}'>
                <meta itemprop='priceCurrency' content='RUB'>";
        } elseif ( $price ) {
            echo "<div class='services-product--price'>{$price}</div>";
            echo "<meta itemprop='price' content='{$price}'>
                <meta itemprop='priceCurrency' content='RUB'>";
        }
?>

        <?php
            if( $price || $price2 ) : ?>
                <div class="services-product--subprice">Цены указаны с НДС 20%</div>
            <?php endif;
        ?>

        <div class="services-product--buttons group">
            <div class="cell size-50">

                <div class="btn-green js-open-order">Заказать газон</div>
            </div>
            <div class="cell size-50">
                <?php get_template_part( 'template-parts/block', 'related-ref' ) ?>
            </div>
        </div>
    </div>
</div>