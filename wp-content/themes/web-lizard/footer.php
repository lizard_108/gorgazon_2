<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
//wp_nav_menu("menu=main-menu");
?>
			<footer>
				<div class="footer--level">
					<div class="group">
						<div class="cell custom-cell">
							<h4 class="js-toggle-ul">Разделы каталога</h4>
                            <?php
                                wp_nav_menu( [

                                    'menu' => 'footer-1',
                                    'container' => '',

                                ] );
                            ?>
						</div>
						<div class="cell custom-cell">
							<h4 class="js-toggle-ul">Услуги</h4>
                            <?php
                            wp_nav_menu( [

                                'menu' => 'footer-2',
                                'container' => ''

                            ] );
                            ?>

						</div>
						<div class="cell custom-cell false-block">
							<h4>&nbsp;</h4>
                            <?php
                            wp_nav_menu( [

                                'menu' => 'footer-3',
                                'container' => ''

                            ] );
                            ?>

						</div>
					</div>
				</div>
				<div class="footer--level two-level">
					<div class="group align-center">
						<div class="cell custom-cell">
							<div class="social">
								Мы в соцсетях: 
								<a href="<?php the_field('fb_ref', 396);?>"><img src="<?=get_stylesheet_directory_uri()?>/images/svg/footer-social-1.svg"></a>
								<a href="<?php the_field('vk_ref', 396);?>"><img src="<?=get_stylesheet_directory_uri()?>/images/svg/footer-social-2.svg"></a>
								<a href="<?php the_field('insta_ref', 396);?>"><img src="<?=get_stylesheet_directory_uri()?>/images/svg/footer-social-3.svg"></a>
							</div>
						</div>
						<div class="cell custom-cell">
							<a href="<?php the_field('footer_price', 396);?>">Скачать прайс лист</a>
						</div>
						<div class="cell custom-cell">
							<a href="<?php the_field('card_ref', 396);?>">Карточка предприятия </a>
						</div>
					</div>
				</div>
				<div class="footer--level last-level">
					<div class="group align-center">
						<div class="cell size-66">
							© Рулонный газон от компании Горгазон. Работать с нами просто!
						</div>
						<div class="cell size-33">
							<a href="<?php the_field('pd_ref', 396);?>">Политика обработки персональных данных</a>
						</div>
					</div>
				</div>
			</footer>
		</div> <!-- end .main -->
		<!-- /// -->
	</div>
	<!-- end .main-container -->
</div>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
</body>
</html>