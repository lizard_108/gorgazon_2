jQuery(document).ready(function($) {

    $('#wp-admin-bar-page-redirects').on('click', function (){

        var clicked_elem = $(this);
        if(clicked_elem.hasClass('ajax-in-progress')){
            return;
        }

        $(clicked_elem).addClass('ajax-in-progress');
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type: 'POST',
            data: {action: 'page_redirects'}
        }).done(function(result){
           if(result){
               var link = document.createElement('a');
               link.setAttribute('href', result);
               link.setAttribute('download', "Редиректы gorgazon" );
               link.click();
               console.log(result);
               return false;
           }
        }).fail(function() {

        }).always(function() {
            clicked_elem.removeClass('ajax-in-progress');
        });
    });

});