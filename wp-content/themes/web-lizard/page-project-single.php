<?php
/**
 * Template Name:  Проекты - страница единичного проекта
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?>
    <main>
        <div class="content-container">
            <?include "inc/search.php"?>
            <?include "inc/breadcrumbs.php"?>
            <div class="main-content">
                <div class="h1 no-caps">Детская площадка</div>
                <ul class="blog-attributes on-blog-page">
                    <li>Укладка</li>
                    <li><a href="">85 м2</a></li>
                </ul>
                <div class="space"></div>

                <div class="blog-page-inner">
                    <?php the_content();?>
                </div>
                <div class="space"></div>
                <div class="space"></div>

                <?
                include 'inc/realize-projects.php';
                ?>
            </div>
        </div>

        <?include "inc/question-form.php"?>
        <?php get_template_part('template-parts/content' );?>
    </main>

<? get_footer(); // подключаем footer.php ?>