
<div class="services-product product-edition">

    <?php get_template_part( "template-parts/servprod-slider" )?>

    <div class="services-product--place">

        <?php echo get_gg_rating_html() ?>

        <div class="space-40"></div>

        <?php get_template_part( 'template-parts/product-plitka', 'height' ) ?>
        <?php get_template_part( 'template-parts/product-plitka', 'colors' ) ?>
        <div class="space-30"></div>

        <?php get_template_part( 'template-parts/product-plitka', 'prokras' ) ?>

        <div class="space-30"></div>

<?php
        $price2 = get_gg_extprice();
        $price = get_gg_price();

        if( $price2 ) {
            echo "<div class='services-product--price'>{$price2}</div>"
            . "<div class='services-product--price-2'>{$price}</div>";
            echo "<meta itemprop='price' content='{$price2}'>
                <meta itemprop='priceCurrency' content='RUB'>";
        } elseif ( $price ) {
            echo "<div class='services-product--price'>{$price}</div>";
            echo "<meta itemprop='price' content='{$price}'>
                <meta itemprop='priceCurrency' content='RUB'>";
        }
?>

        <?php
            if( $price || $price2 ) : ?>
                <div class="services-product--subprice">Цены указаны с НДС 20%</div>
            <?php endif;
        ?>

        <div class="services-product--subprice">Товар продается кратно поддону по 13.176 кв.м</div>
        <div class="services-product--buttons group">
            <div class="cell size-50">
                <div class="btn-green js-open-order-plitka">Заказать плитку</div>
            </div>
            <div class="cell size-50">
                <?php get_template_part( 'template-parts/block', 'related-ref' ) ?>
            </div>
        </div>

    </div>
</div>