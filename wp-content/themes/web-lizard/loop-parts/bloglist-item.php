<?php

    $cats = [];
    $cats_name = [];
    $post_cats = get_the_category();

    foreach ( $post_cats as $cat ) {
        if ( ! in_array( $cat->slug, $cats ) ) {
            $cats[] = $cat->slug;
            $cats_name[$cat->slug] = $cat->name;
        }
    }

    $cats_str = implode(',', $cats );
    if( $cats_str ) {
        $add_data = "data-cats='{$cats_str}' ";
    }

    $cats_list = '';
    foreach ( $cats_name as $cat ){
        $cats_list .= "<li {$add_data}>{$cat}</li>";
    }

?>

<div <?=$add_data?> class="blog-list-item visible">
    <div class="blog-list-item--image">
        <a href="<?php the_permalink()?>"><img src="<?php the_post_thumbnail_url() ?>" alt=""></a>
    </div>
    <div class="blog-list-item--place">
        <?php echo get_gg_rating_html()?>
        <div class="title"><a href="<?php the_permalink()?>"><?php the_title()?></a></div>
        <?php the_field('description_short');?>
        <ul class="blog-attributes">
            <?=$cats_list?>
            <li><?php echo get_the_date('j F Y')?></li>
            <li>Горгазон</li>
        </ul>
    </div>
</div>