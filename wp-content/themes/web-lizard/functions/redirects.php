<?php

add_action('wp_ajax_page_redirects', 'page_redirects');
function page_redirects() {
    $args = array(
        'post_type'         => 'page',
        'posts_per_page'    => '-1',
        'post_status'       => 'publish',
        'fields'            => 'ids',
        'orderby'           => 'date',
        'order'             => 'DESC',
    );

    $results = new WP_Query( $args );
    $redirects = "RewriteEngine On\r\n";
    $redirects .= "##\r\n## блок редиректов для страниц \r\n##\r\n";

    foreach ( $results->posts as $res ) {
        $old_url = get_field( 'old_url', $res );
        if( $old_url && $old_url != 'нет' ){
            $old_urls = explode( ',', $old_url );
            $url_to = get_permalink( $res );
            foreach ( $old_urls as $url ){
                $url_from = parse_url( trim( $url ) );
                $url_to_arr = parse_url( trim( $url_to ) );
                if( ( $url_from['path'] ) && ( trim( $url_from['path'], '/' ) != trim( $url_to_arr['path'], '/' ) ) ) {
                    $from = trim($url_from['path'],"/");
                    $redirects .=  "Redirect 301 /{$from} {$url_to}\r\n";
                }

            }

        }
    }

    $args = array(
        'post_type'         => 'attachment',
        'posts_per_page'    => '-1',
        'post_status'       => ['publish', 'inherit'],
        'orderby'           => 'date',
        'order'             => 'DESC',
    );

    $results = new WP_Query( $args );
    $redirects .= "##\r\n## блок редиректов для изображений \r\n##\r\n";
    foreach ( $results->posts as $res ) {
        $old_url = $res->post_content;
        if( $old_url ){
            $url_from = parse_url( $old_url );
            $url_to = wp_get_attachment_image_url( $res->ID, 'full' );
            $redirects .=  "Redirect 301 {$url_from['path']} {$url_to}\r\n";
        }
    }

    $fpath = get_stylesheet_directory() . '/data/outputs/';
    $furi = get_stylesheet_directory_uri() . '/data/outputs/';
    $fname = 'redirects.txt';

    $fullname = $fpath . $fname;
    $fulluri = $furi . $fname;

    unlink( $fullname );
    if ( file_put_contents( $fullname, $redirects, LOCK_EX ) ) {
        echo $fulluri;
    }

    wp_die();
}

function redirects(){
    $args = array(
        'post_type'         => 'attachment',
        'post_status'       => ['publish', 'inherit'],
        'posts_per_page'    => '-1',
        'orderby'           => 'date',
        'order'             => 'DESC',
    );

    $results = new WP_Query( $args );
    //echo "<pre>"; print_r($results); echo "</pre>";
    $redirects = "##\r\n## блок редиректов для изображений \r\n##\r\n";
    foreach ( $results->posts as $res ) {
        echo "<pre>"; print_r($res); echo "</pre>";
        $old_url = $res->post_content;
        if( $old_url ){
            $url_from = parse_url( $old_url );
            $url_to = wp_get_attachment_image_url( $res->ID );
            $redirects .=  "Redirect 301 {$url_from['path']} {$url_to}\r\n";
        }
    }
    echo $redirects;

}
