<?php
/**
 * Template Name: страница "Контакты"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>

        <?php get_template_part('template-parts/insert1' ) ?>
		
		<div class="main-content content-wrap">
			<div class="h1 h2-link">Контакты <a href="<?php echo home_url() . '/predpriyatie/';?>">Карточка предприятия</a></div>
            <?php get_template_part('template-parts/content2' ) ?>
		</div>

        <?php get_template_part('template-parts/insert2' ) ?>

	</div>


    <?php get_template_part('template-parts/insert3' ) ?>
</main>

<?php get_footer() ?>