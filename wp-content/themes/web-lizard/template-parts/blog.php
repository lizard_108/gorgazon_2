<div class="h2 h2-link">Блог <a href="">все статьи</a></div>

<div class="blog-list">
    <?
    for ($i=0; $i < 2; $i++) { ?>
        <div class="blog-list-item">
            <div class="blog-list-item--image">
                <img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-item.png" alt="">
            </div>
            <div class="blog-list-item--place">
                <div class="rating"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
                <div class="title">Правильная укладка рулонного газона</div>
                <p>Универсальный газон, подходит для широкого спектра озеленения</p>
                <ul class="blog-attributes">
                    <li><a href="">Укладка</a></li>
                    <li>12 декабря 2019</li>
                    <li>Горгазон</li>
                </ul>
            </div>
        </div>
    <?}?>
</div>