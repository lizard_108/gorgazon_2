<?php
// delivery 2 level


    $subprod_args = array(
        'post_type'         => 'page',
        'posts_per_page'    => '-1',
        'post_status'       => 'publish',
        'order'             => 'ASC',
        'post_parent'       => get_the_ID(),
    );

    $subproducts = new WP_Query( $subprod_args );

    if( $subproducts->post_count ) : ?>

        <div class='delivery-list'>
            <div class='group'>

                <div class='cell size-50'>
                    <ul class='article-list'>
                        <?php
                            $rows_count = get_rows_in_col( $subproducts->post_count );
                            $i = 0;
                            while ( $subproducts->have_posts() ) {
                                if( ++$i > $rows_count ) {
                                    break;
                                }
                                $subproducts->the_post();
                                get_template_part('loop-parts/delivery', 'item');
                            }
                        ?>
                    </ul>
                </div>

                <div class='cell size-50'>
                    <ul class='article-list'>
                        <?php
                            while ( $subproducts->have_posts() ) {
                                $subproducts->the_post();
                                get_template_part('loop-parts/delivery', 'item');
                            }
                        ?>
                    </ul>
                </div>

            </div>
        </div>
    <?php endif;

    wp_reset_postdata();
