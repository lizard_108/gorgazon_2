<?  // faq loop

    $args = array(
        'post_type' => 'faq',
        'posts_per_page' => '-1',
        'post_status' => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
    );

    $posts = new WP_Query($args);

    $cats = [];

    if ($posts->have_posts()) : while ($posts->have_posts()) : $posts->the_post();

        $post_cats = get_the_category();

        foreach ( $post_cats as $cat ) {
            if ( ! in_array( $cat->name, $cats ) ) {
                $cats[$cat->slug] = $cat->name;
            }
        }

    endwhile;

        natcasesort($cats);
        $all_cats = implode(',', $cats );
        echo "<ul id='faq-tab-switcher' class='tab-switcher'>
                <li data-cat='all' class = 'on'>Все</li>";

        foreach ( $cats as $key => $cat ) {
            echo "<li data-cat='{$key}'>{$cat}</li>";
        }
        echo "</ul>";

        wp_reset_postdata();
    else: ?>

        <p><?php _e('Нет записей'); ?></p>

    <?php endif;