<?php

    $descr =  get_field( 'intro_text' ); // old field: service_short_descr_single
    if ( $descr ) {
        echo "<div class='content-wrap'>" . apply_filters( 'the_content', $descr ) . "</div>";
    } else {
        if( is_user_role_in(['administrator','editor']) ){
            echo "<div class='inserts'>КРАТКИЙ ВСТУПИТЕЛЬНЫЙ ТЕКСТ</div>";
        }
    }
