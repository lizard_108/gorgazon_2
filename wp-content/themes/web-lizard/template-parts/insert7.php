<?php

    $ins =  get_field('insert7');
    if ( is_a( $ins, 'WP_Post' ) ) {
        echo $ins->post_content;
    } else {
        if( is_user_role_in(['administrator','editor']) ){
            echo "<div class='inserts'>ВСТАВКА 7</div>";
        }
    }