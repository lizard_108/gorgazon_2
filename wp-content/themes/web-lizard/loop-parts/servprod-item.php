
<div class='services-list--item'>

    <div class='services-list--img'>
        <a href='<?php the_permalink() ?>'>
            <img src="<?php the_post_thumbnail_url('thumbnail') ?>" alt="">
        </a>
    </div>
    <div class='service-list--text'>
        <div class="h3"><a href='<?php the_permalink() ?>'><?php the_title() ?></a></div>
        <div class="service-list--description"><?php echo apply_filters('the_content', get_field( 'description_short' ) ) ?></div>
        <div class='show-more-link'><a href='<?php the_permalink() ?>'>Подробнее</a></div>
    </div>

    <div class='service-list--price'>
        <?php

            $price =  get_gg_price();
            if( $price ) {
                echo "{$price}<br><small>Цены указаны с НДС 20%</small>";
            }

        ?>
    </div>

</div>