<?  // project pages loop

    $args = array(
        'post_type' => 'page',
        'posts_per_page' => '-1',
        'post_status' => 'publish',
        'order' => 'ASC',
        'post_parent' => 320,
    );

    $posts = new WP_Query( $args );

    if ( $posts->have_posts() ) : while ( $posts->have_posts() ) : $posts->the_post();

        get_template_part('loop-parts/project', 'item');

    endwhile;
        wp_reset_postdata();
    else: ?>

        <p><?php _e('Нет записей'); ?></p>

    <?php endif;