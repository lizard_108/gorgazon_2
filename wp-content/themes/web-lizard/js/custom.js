$(document).ready(function() {

	$('select').styler();

	$('.input--phone').mask('+0 (000) 000 00 00', {placeholder: "+_ (___) ___ __ __"});


	$(".js-open-sandwich").click(function(event) {
		$(".adapt-header .sandwich-open").slideToggle(108);
		blur(true);
	});
	$(".js-close-sandwich").click(function(event) {
		$(".adapt-header .sandwich-open").slideToggle(108);
		blur(false);
	});


	$(".js-open-adapt-search").click(function(event) {

		$(this).toggleClass('on');
		$(".adapt-search-open").toggle();

		if ($(this).hasClass('on')) {
			blur(true);
		} else {
			blur(false);
		}

		return false;
	});

	$(".js-open-adapt-share").click(function(event) {

		$(this).toggleClass('on');
		$(".adapt-share-open").toggle();

		if ($(this).hasClass('on')) {
			blur(true);
		} else {
			blur(false);
		}
		
		return false;
	});

	$(".js-open-share").click(function(event) {
		/* Act on the event */
		$(this).toggleClass('on');

		if ($(this).hasClass('active')) {
			
		} else {
			$(".share-open").toggle();

			setTimeout(clickk, 108);

			function clickk () {
			    $(".js-open-share").addClass("active");
			}
		}
	});

	$(document).click(function (e) {
	    if ($(e.target).is('.share-open')) {} else {
	    	if ($(".js-open-share").hasClass('active')) {
		        $(".share-open").hide();
		        $(".js-open-share").removeClass('on');
		        $(".js-open-share").removeClass('active');
	    	}
	    }
	});

	$(".js-toggle-ul").click(function(event) {
		if ($(window).width() <= 768) {
			$(this).closest('.cell').find('ul').slideToggle(333);
		}
	});

	// open modal windows


	$(".js-add-review").click(function(event) {
		blur(true);
		$(".modal-window--add-review").show();
		return false;
	});

	$(".js-open-calc").click(function(event) {
		/* Act on the event */
		blur(true);
		$(".modal-window--calc").show();
		return false;		
	});

	$(".js-open-callback-form").click(function(event) {
		blur(true);

		$('#callback-form').show();
		$('.modal-window--message-after-send').hide();
		$(".modal-window--callback").show();
		return false;
	});

	$(".js-open-order").click(function(event) {
		blur(true);
		$('#callback-order-gazon-form').show();
		$('.modal-window--message-after-send').hide();
		$(".modal-window--order").show();
		return false;
	});

	$(".js-open-order-plitka").click(function(event) {
		blur(true);
		$('#callback-order-plitka-form').show();
		$('.modal-window--message-after-send').hide();
		$(".modal-window--order-plitka").show();
		return false;
	});

	$(".menu-item-400 > a").click(function(event) {
		/* Act on the event */
		$(this).closest('.menu-item-400').toggleClass('current_page_item');
		return false;
	});

	
	// end open modal windows

	$(".window-blur, .modal-window .close, .calc--close, .js-close-modal-windows").click(function(event) {
		$(".window-blur").hide();
		$(".adapt-header--buttons a").removeClass('on');
		$(".adapt-share-open, .modal-window, .adapt-search-open").hide();
	});


	$("#faq-tab-switcher").on('click', 'li', function(event) {
		var cat = $(this).attr('data-cat');
		$(this).addClass('on').siblings().removeClass('on');

		$(".hidden-sections .section .faq-list--item").each(function (){
			if(($.inArray(cat, $(this).attr('data-cats').split(','))!=-1)||(cat=='all')){
				$(this).addClass('visible');
			} else{
				$(this).removeClass('visible');
			}
		});
	});

	$("#blog-tab-switcher").on('click', 'li', function(event) {
		var cat = $(this).attr('data-cat');
		$(this).addClass('on').siblings().removeClass('on');

		$(".blog-list .blog-list--item").each(function (){
			if(($.inArray(cat, $(this).attr('data-cats').split(','))!=-1)||(cat=='all')){
				$(this).addClass('visible');
			} else{
				$(this).removeClass('visible');
			}
		});
	});

	$("#servprod-tab-switcher").on('click', 'li', function(){
		var tabname = $(this).attr('data-open');
		$(this).addClass('on').siblings().removeClass('on');

		$(".hidden-sections .section").each(function (){
			if($(this).attr('data-tabname') == tabname ){
				$(this).siblings().hide();
				$(this).slideDown(333);

				return false;
			}
		});

	});

	$(".js-article-determined-block--open-seo a").click(function(event) {
		$(".article-determined-block--hidden").slideToggle(333);
		$(this).toggleClass('on');
		if ($(this).hasClass('on')) {
			$(this).html("Показать меньше");
		} else {
			$(this).html("Показать показать больше");			
		}
		return false;		
	});

	$(".js-open-comment-form").click(function(event) {
		$(".add-comment--open-form").slideDown(333);
		$(this).hide();
	});

	


	// dynamic-input

	$(".dynamic-input").click(function(event) {
		$(this).addClass('focus');
	});

	$(".dynamic-input input").blur(function(event) {
		if ($(this).val()) {
			$(this).closest('.dynamic-input').addClass('focus');
		} else {
			$(this).closest('.dynamic-input').removeClass('focus');			
		}
	});

	$(".faq-list--item").click(function(event) {
		$(this).toggleClass('open');
	});

	// dynamic-input end

	// module stars 

	$(".module-stars").each(function(index, el) {

		rating = parseFloat($(this).attr('data-rating'));
		if(isNaN(rating)){
			rating = 0;
		}

		var percent_rating = 0;

		if(rating>0){
			percent_rating = 10;
		}
		if(rating>0.75){
			percent_rating = 20;
		}
		if(rating>1.25){
			percent_rating = 30;
		}
		if(rating>1.75){
			percent_rating = 40;
		}
		if(rating>2.25){
			percent_rating = 50;
		}
		if(rating>2.75){
			percent_rating = 60;
		}
		if(rating>3.25){
			percent_rating = 72;
		}
		if(rating>3.75){
			percent_rating = 80;
		}
		if(rating>4.25){
			percent_rating = 92;
		}
		if(rating>4.75){
			percent_rating = 100;
		}



/*
		switch (rating) {
			case "5":

				break;
			case "4.5":
				percent_rating = 92;
				break;
			case "4":
				percent_rating = 80;
				break;
			case "3.5":
				percent_rating = 72;
				break;
			case "3":
				percent_rating = 60;
				break;
			case "2.5":
				percent_rating = 52;
				break;
			case "2":
				percent_rating = 40;
				break;
			case "1.5":
				percent_rating = 30;
				break;
			case "1":
				percent_rating = 20;
				break;
			case ".5":
				percent_rating = 10;
				break;
			case "0":
				percent_rating = 0;
				break;
		}

 */
		$(this).find('.module-stars--control').css("width", percent_rating+"%");
	});

	$(".module-stars.active").mousemove(function(e) {
		var offset = $(this).offset();
		var relativeX = (e.pageX - offset.left);

		var total_width = $(this).width();
		var relativeX_percent = (relativeX / total_width) * 100;
		var current_percent = Math.round(relativeX_percent);
		var rating;

		if (current_percent >= 0) {
			rating = ".5";
		}
		if (current_percent >= 10) {
			rating = "1";
		}
		if (current_percent >= 20) {
			rating = "1.5";
		}
		if (current_percent >= 30) {
			rating = "2";
		}
		if (current_percent >= 40) {
			rating = "2.5";
		}
		if (current_percent >= 52) {
			rating = "3";
		}
		if (current_percent >= 60) {
			rating = "3.5";
		}
		if (current_percent >= 72) {
			rating = "4";
		}
		if (current_percent >= 80) {
			rating = "4.5";
		}
		if (current_percent >= 92) {
			rating = "5";
		}

		switch (rating) {
			case "5":
				percent_rating = 100;
				break;
			case "4.5":
				percent_rating = 92;
				break;
			case "4":
				percent_rating = 80;
				break;
			case "3.5":
				percent_rating = 72;
				break;
			case "3":
				percent_rating = 60;
				break;
			case "2.5":
				percent_rating = 52;
				break;
			case "2":
				percent_rating = 40;
				break;
			case "1.5":
				percent_rating = 30;
				break;
			case "1":
				percent_rating = 20;
				break;
			case ".5":
				percent_rating = 10;
				break;
			case "0":
				percent_rating = 0;
				break;
		}

		$(this).find('.module-stars--control').css("width", percent_rating+"%");
		$(this).find('.module-stars--control').attr("data-rating", rating);
	});

	// module stars end


	// calc 

	$(".js-open-adapt-calc-results").click(function(event) {
		$(".calc--right-side .gray-place").slideDown(333);
	});

	$(".js-close-adapt-calc-results").click(function(event) {
		$(".calc--right-side .gray-place").slideUp(333);
	});

	// calc end

	$(window).scroll(function(event) {
		//console.log(getBodyScrollTop());
		if (getBodyScrollTop() >= 1000) {
			$(".header-banner").addClass('fix');
		} else {
			$(".header-banner").removeClass('fix');
		}
	});

	function blur($toggle) {
		if ($toggle) {
			$(".window-blur").show();
		} else {
			$(".window-blur").hide();
		}
	}

	var share = {

		twitter: function($this){

			var data = share.data($this);

			if(data){

				var url  = "http://twitter.com/share?";
				url += "text="      + encodeURIComponent(data.text);
				url += "&url="      + encodeURIComponent(data.url);
				url += "&hashtags=" + "";
				url += "&counturl=" + encodeURIComponent(data.url);

				share.popup(url);
			};

			return false;
		},
		vk: function($this){

			var data = share.data($this);

			if(data){

				var url  = "http://vkontakte.ru/share.php?";
				url += "url="          + encodeURIComponent(data.url);
				url += "&title="       + encodeURIComponent(data.title);
				url += "&description=" + encodeURIComponent(data.text);
				url += "&image="       + encodeURIComponent(data.img);
				url += "&noparse=true";

				share.popup(url);
			};

			return false;
		},
		facebook: function($this){

			var data = share.data($this);

			if(data){

				var url  = "http://www.facebook.com/sharer.php?s=100";
				url += "&p[title]="     + encodeURIComponent(data.title);
				url += "&p[summary]="   + encodeURIComponent(data.text);
				url += "&p[url]="       + encodeURIComponent(data.url);
				url += "&p[images][0]=" + encodeURIComponent(data.img);

				share.popup(url);
			};

			return false;
		},
		data: function($this){

			if($this){

				return $.parseJSON($this.parent("div").attr("data-share-data"));
			};

			return false;
		},
		popup: function(url){

			window.open(url, "", "toolbar=0, status=0, width=626, height=436");

			return false;
		}
	};

	getBodyScrollTop = function() {
		return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
	};

	// обработка форм обратной связи

	$("#callback-form").on('click', '.btn-green', function(){
		var clicked_elem = $(this);
		if(clicked_elem.hasClass('ajax-in-progress')){
			return;
		}
		$(clicked_elem).addClass('ajax-in-progress');

		var formData = {};

		formData.phone = $("#callback-form input#callback-phone").val();
		formData.name = $("#callback-form input#callback-name").val();

		if(formData.phone==''){
			$("#callback-form input#callback-phone").parent().addClass('validate-error');
			return false;
		}

		if(formData.name==''){
			$("#callback-form input#callback-name").parent().addClass('validate-error');
			return false;
		}

		$.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: 'POST',
			data: {action: 'send_callback_form', formData: JSON.stringify(formData)}
		}).done(function(result){

			if(result=='success'){
				$("#callback-form").hide();
				$('.modal-window--message-after-send').show();
			} else {
				alert('Произошла ошибка при отправке. Перезагрузите страницу и попробуйте еще раз.');
			}

		}).fail(function() {
			alert('Что-то пошло не так. Перезагрузите страницу и попробуйте еще раз.');
		}).always(function() {
			clicked_elem.removeClass('ajax-in-progress');
		});
	});

	$("#callback-inline-form").on('click', '.btn-green', function(){
		var clicked_elem = $(this);
		if(clicked_elem.hasClass('ajax-in-progress')){
			return;
		}
		$(clicked_elem).addClass('ajax-in-progress');

		var formData = {};

		formData.phone = $("#callback-inline-form input#callback-inline-phone").val();
		formData.name = $("#callback-inline-form input#callback-inline-name").val();

		if(formData.phone==''){
			$("#callback-inline-form input#callback-inline-phone").parent().addClass('validate-error');
			return false;
		}

		if(formData.name==''){
			$("#callback-inline-form input#callback-inline-name").parent().addClass('validate-error');
			return false;
		}

		$.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: 'POST',
			data: {action: 'send_callback_form', formData: JSON.stringify(formData)}
		}).done(function(result){

			if(result=='success'){
				$(".modal-window--callback #callback-form").hide();
				$('.modal-window--callback  .modal-window--message-after-send').show().parent().show();
				$("#callback-inline-form input#callback-inline-phone").val('');
				$("#callback-inline-form input#callback-inline-name").val('');

			} else {
				alert('Произошла ошибка при отправке. Перезагрузите страницу и попробуйте еще раз.');
			}

		}).fail(function() {
			alert('Что-то пошло не так. Перезагрузите страницу и попробуйте еще раз.');
		}).always(function() {
			clicked_elem.removeClass('ajax-in-progress');
		});
	});

	$("form input").on('focus', function() {
		$(this).removeClass('validate-error');
	})

	$("#callback-order-gazon-form").on('click', '.btn-green', function(){

		var formData = {};

		formData.phone = $("#callback-order-gazon-form input#gazon-order-phone").val();
		formData.square = $("#callback-order-gazon-form input#gazon-order-square").val();
		formData.name = $("#callback-order-gazon-form input#gazon-order-name").val();
		formData.comment = $("#callback-order-gazon-form textarea#gazon-order-comment").val();
		formData.url = document.location.pathname;


		if(formData.phone==''){
			$("#callback-order-gazon-form input#gazon-order-phone").parent().addClass('validate-error');
			return false;
		}

		if(formData.square==''){
			$("#callback-order-gazon-form input#gazon-order-square").parent().addClass('validate-error');
			return false;
		}

		if(formData.name==''){
			$("#callback-order-gazon-form input#gazon-order-name").parent().addClass('validate-error');
			return false;
		}

		var clicked_elem = $(this);
		if(clicked_elem.hasClass('ajax-in-progress')){
			return;
		}
		$(clicked_elem).addClass('ajax-in-progress');

		$.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: 'POST',
			data: {action: 'send_gazon_order_form', formData: JSON.stringify(formData)}
		}).done(function(result){

			if(result=='success'){
				$("#callback-order-gazon-form").hide();
				$('.modal-window--message-after-send').show();
			} else {
				alert('Произошла ошибка при отправке. Перезагрузите страницу и попробуйте еще раз.');
			}

		}).fail(function() {
			alert('Что-то пошло не так. Перезагрузите страницу и попробуйте еще раз.');
		}).always(function() {
			clicked_elem.removeClass('ajax-in-progress');
		});
	});

	$("#callback-order-plitka-form").on('click', '.btn-green', function(){

		var formData = {};

		formData.phone = $("#callback-order-plitka-form input#plitka-order-phone").val();
		formData.square = $("#callback-order-plitka-form input#plitka-order-square").val();
		formData.name = $("#callback-order-plitka-form input#plitka-order-name").val();
		formData.comment = $("#callback-order-plitka-form textarea#plitka-order-comment").val();
		formData.slug = document.location.pathname;

		if(formData.phone==''){
			$("#callback-order-plitka-form input#plitka-order-phone").parent().addClass('validate-error');
			return false;
		}

		if(formData.square==''){
			$("#callback-order-plitka-form input#plitka-order-square").parent().addClass('validate-error');
			return false;
		}

		if(formData.name==''){
			$("#callback-order-plitka-form input#plitka-order-name").parent().addClass('validate-error');
			return false;
		}

		var clicked_elem = $(this);
		if(clicked_elem.hasClass('ajax-in-progress')){
			return;
		}
		$(clicked_elem).addClass('ajax-in-progress');

		$.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: 'POST',
			data: {action: 'send_plitka_order_form', formData: JSON.stringify(formData)}
		}).done(function(result){

			if(result=='success'){
				$("#callback-order-plitka-form").hide();
				$('.modal-window--message-after-send').show();
			} else {
				alert('Произошла ошибка при отправке. Перезагрузите страницу и попробуйте еще раз.');
			}

		}).fail(function() {
			alert('Что-то пошло не так. Перезагрузите страницу и попробуйте еще раз.');
		}).always(function() {
			clicked_elem.removeClass('ajax-in-progress');
		});
	});

});