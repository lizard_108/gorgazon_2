<div class="realize-projects">
    <div class="h2 h2-link">Реализованные проекты <a href="<?php home_url() ?>/proekty/">все проекты</a></div>
    <div class="slider--wr">
        <div class="realize-projects--slider">

            <?php get_template_part('loops/projects' ) ?>

        </div>
    </div>
</div>
<div class="show-mobile all-list-link"><a href="">Все проекты</a></div>