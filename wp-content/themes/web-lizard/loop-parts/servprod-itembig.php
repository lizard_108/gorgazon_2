
    <div class="services-list-2--item">


            <div class="services-list-2--img">
                <a href="<?php the_permalink() ?>">

                    <?php
                        echo kama_thumb_img( array(
                            'src' => get_the_post_thumbnail_url(),
                            'w' => 435,
                            'h' => 260,
                        ) );
                    ?>
                </a>
            </div>


        <div class="services-list-2--text">

            <div class="h3"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></div>
            <p><?php the_field( 'description_short' ) ?></p>

            <?php echo get_gg_rating_html( '',  'size-middle', get_field('rating_avg'), get_field('rating_count') );
                $price2 = get_gg_extprice();
                $price = get_gg_price();

                echo "<div class='services-list-2--price'>{$price2}</div>";
                echo "<div class='services-list-2--price-2'>{$price}</div>";

            ?>

            <div class="group">
                <div class="cell size-50">
                <?php
                    if( $price || $price2 ) {
                        echo "Цены указаны с НДС 20%";
                    }
                ?>
                </div>

                <div class="cell size-50"><div class="show-more-link"><a href="<?php the_permalink() ?>">Подробнее</a></div></div>
            </div>

        </div>
    </div>
