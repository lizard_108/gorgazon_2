
<?php
    $imgurl = kama_thumb_src( array(
        'src' => get_the_post_thumbnail_url( $id, 'full' ),
        'w' => 435,
        'h' => 290,
    ) );
?>

<div class="services-product">

    <?php get_template_part( "template-parts/servprod-slider" )?>

    <div class="services-product--place">
        <?php echo get_gg_rating_html()?>
        <div class="services-product--term">
            <?php apply_filters( 'the_title', the_field( 'description_short_card' ) ); ?>
        </div>
        <div class="services-product--price">
            <?php
                $price = get_gg_price();
                echo $price;
            ?>
        </div>
        <div class="services-product--subprice">
            <?php
                if($price){
                    echo "Цены указаны с НДС 20%";
                }
            ?>
        </div>
        <div class="services-product--buttons group">
            <div class="cell size-50">
                <div class="btn-green js-open-order">
                    <?php
                        $btn_text = get_field('order_btn_text');
                        echo ($btn_text ? $btn_text : 'Заказать укладку');
                    ?>
                </div>
            </div>
            <div class="cell size-50">
                <?php get_template_part( 'template-parts/block', 'related-ref' ) ?>
            </div>
        </div>
    </div>
</div>